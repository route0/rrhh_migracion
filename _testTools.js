const mongoose = require('mongoose');


// Pruebas con mongoose para guardar imagenes
const models = require("./schemas/models");


mongoose.connect('mongodb://localhost:27017/rrhh', {useNewUrlParser: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    // readAndSave();
});


const gridfs = require('mongoose-gridfs');
const { Readable } = require('stream');

// async function readAndSave(){
//     console.log('Guardando Imagen');
//     await pool.connect();

//     test_mssql();
// }

function makeFs() {
    const AgentesImagenesSchema = gridfs({
        collection: 'AgentesImagenes',
        model: 'AgentesImagenes',
        mongooseConnection: mongoose.connection
    }); 
    return AgentesImagenesSchema.model;
}

async function saveImage(agente){
    console.log('Guardando Imagen');
    console.log(agente);
    var stream = new Readable();
    var buffer = Buffer.from(agente.foto, 'base64');
    stream.push(buffer);
    stream.push(null);
    const agenteFotoModel = makeFs();
    const options = ({ filename: 'test_base64.jpg', contentType: 'image/jpg' });
    agenteFotoModel.write(options, stream, (error, file) => { });
}