const logger = require("../logger").logger;
const {
	Ubicacion,
	ParteEstado,
	ParteJustificacion,
	Parte,
	ParteAgente,
	Agente,
	Fichada,
	FichadaCache,
	Articulo,
	Franco,
} = require("./models");

async function getUbicacionByCodigo(codigo) {
	try {
		return await Ubicacion.findOne({ codigo: codigo });
	} catch (err) {
		logger.debug(`No se encontró Ubicacion con codigo ${codigo}`);
		return null;
	}
}

async function getParteEstadoByNombre(nombre) {
	try {
		return await ParteEstado.findOne({ nombre: nombre });
	} catch (err) {
		logger.debug(`No se encontró Ubicacion con nombre ${nombre}`);
		return null;
	}
}

async function getParteJustificacionByNombre(nombre) {
	try {
		return await ParteJustificacion.findOne({ nombre: nombre });
	} catch (err) {
		logger.debug(`No se encontró ParteJustificacion con nombre ${nombre}`);
		return null;
	}
}

async function addParte(obj) {
	try {
		let parte = new Parte(obj);
		return await parte.save();
	} catch (err) {
		logger.debug(`No se pudo insertar el parte siguiente ${parte}`);
		return null;
	}
}

async function addFichadasAgente(agente, fichadas) {
	try {
		return await Fichada.insertMany(fichadas);
	} catch (err) {
		logger.debug(
			`No se pudieron insertar fichadas del agente numero ${agente.numero}`
		);
		return;
	}
}

async function addFichadasCacheAgente(agente, fichadas) {
	try {
		return await FichadaCache.insertMany(fichadas);
	} catch (err) {
		logger.debug(
			`No se pudieron insertar fichadas cache del agente numero ${agente.numero}`
		);
		return;
	}
}

async function addPartesAgentes(parte, partesAgentes) {
	try {
		return await ParteAgente.insertMany(partesAgentes);
	} catch (err) {
		logger.debug(
			`No se pudieron insertar los partes de los agentes del parte ${parte}`
		);
		return;
	}
}

async function getAgenteByNumero(numero) {
	try {
		return await Agente.findOne({ numero: numero });
	} catch (err) {
		logger.debug(`No se encontró el agente con numero ${numero}`);
		return null;
	}
}

async function getArticuloByCodigo(codigo) {
	try {
		return await Articulo.findOne({ codigo: codigo });
	} catch (err) {
		logger.debug(`No se encontró Articulo con codigo ${codigo}`);
		return null;
	}
}

async function addFrancosAgentes(agente, francosAgentes) {
	try {
		return await Franco.insertMany(francosAgentes);
	} catch (err) {
		logger.debug(
			`No se pudieron insertar Francos del agente numero ${agente.numero}`
		);
		return;
	}
}

async function addUbicaciones(ubicaciones) {
	try {
		return await Ubicacion.insertMany(ubicaciones);
	} catch (err) {
		logger.debug(`No se pudieron insertar masivamente las ubicaciones`);
		return;
	}
}

module.exports = {
	getAgenteByNumero: getAgenteByNumero,
	getUbicacionByCodigo: getUbicacionByCodigo,
	getParteEstadoByNombre: getParteEstadoByNombre,
	getParteJustificacionByNombre: getParteJustificacionByNombre,
	addParte: addParte,
	addPartesAgentes: addPartesAgentes,
	addFichadasAgente: addFichadasAgente,
	addFichadasCacheAgente: addFichadasCacheAgente,
	getArticuloByCodigo: getArticuloByCodigo,
	addFrancosAgentes: addFrancosAgentes,
	addUbicaciones: addUbicaciones,
};
