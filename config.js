function env(key, _default, type = 's') {
    if (!!process.env[key] === false) {
        return _default;
    }
    const value = process.env[key];
    switch (type) {
        case 'b':
            return value.toLowerCase() === 'true';
        case 'n':
            return parseInt(value, 10);
        default:
            return value;
    }
}


module.exports = {
    version: '2.0',
    api: {
        url: env('API_URL', 'http://mongolito.hospitalneuquen.org.ar:8070/api/')
    },
    db: {
        sqlserver: {
            server: env('SQLSERVER_SERVER', '172.16.1.79'),
            database: env('SQLSERVER_DB', 'Hospital'),
            user: env('SQLSERVER_USER', 'danievas'),
            password: env('SQLSERVER_PASS', 'PeThiSha'),
        },
        mongo: {
            url: env('MONGO_HOST', 'mongodb://mongolito.hospitalneuquen.org.ar:27028'),
            database: env('MONGO_DB', 'rrhh'),
            uri : env('MONGO_URI', "mongodb://admin:golitoMon04@mongolito.hospitalneuquen.org.ar:27028/rrhh?authSource=admin")
        } 
    },
    userMigration: {
        user: '11111111',
        password: '11111111',
    }

}
