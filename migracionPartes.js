const { sqlDB } = require("./db");
const api = require("./api");
const utils = require("./utils");
const logger = require("./logger").logger;
const controller = require("./schemas/controller");

async function migrateAuxiliarTables() {
	await migrateParteEstado();
	await migrateParteJustificacion();
	await migrateUbicacionTipo();
	await migrateUbicacionSubtipo();
	await migrateUbicacion(); // Son alrededor de 21 mil registros
}

async function migrateParteEstado() {
	const schema = "ParteEstado";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT nombre
            FROM Personal_PartesDiarios_Estados`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseParteEstado(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateParteJustificacion() {
	const schema = "Personal_PartesDiarios_Opciones";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT nombre, hint
            FROM Personal_PartesDiarios_Opciones`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseParteJustificacion(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateUbicacionTipo() {
	const schema = "UbicacionTipo";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
                SELECT Codigo, Nombre
            FROM Ubicaciones_Tipos`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseUbicacionTipo(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateUbicacionSubtipo() {
	const schema = "UbicacionSubtipo";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
                SELECT Codigo, Nombre
            FROM Ubicaciones_Subtipos`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseUbicacionSubtipo(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function findUbicacionAncestors(leafID) {
	try {
		let ancestors = [];
		const request = sqlDB.request();
		const result = await request.query`
                WITH Ancestors AS
                (
                    SELECT  Codigo, 
                            Padre 
                    FROM    Ubicaciones 
                    WHERE   Codigo = ${leafID}
                    UNION ALL
                    SELECT  u.Codigo , 
                            u.Padre 
                    FROM    Ubicaciones u
                            INNER JOIN Ancestors a ON a.Padre = u.Codigo 
                )
                SELECT  Codigo
                FROM    Ancestors;`;
		if (result.recordset && result.recordset.length) {
			for (let index = 0; index < result.recordset.length; index++) {
				if (index != 0) {
					ancestors.unshift(result.recordset[index].Codigo);
				}
			}
		}
		return ancestors;
	} catch (err) {
		logger.error(`Error consultar ancestros ubicacion id ${leafID}!!!`);
		logger.error(err);
		return [];
	}
}

async function migrateUbicacion() {
	const schema = "Ubicacion";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		let offset = 0;
		const nextrows = 500;
		const totalRowsQuery = await request.query(
			"SELECT COUNT(*) total FROM Ubicaciones"
		);
		const totalRows = totalRowsQuery.recordset[0].total;
		while (totalRows >= offset) {
			const result = await request.query`
                SELECT Codigo, Padre, Nombre, NombreCorto,
                Interno, Tipo, Subtipo, Telefono,
                Despacho_Habilitado
                FROM Ubicaciones
                ORDER BY Codigo
                OFFSET ${offset} ROWS
                FETCH NEXT ${nextrows} ROWS ONLY`;
			if (result.recordset && result.recordset.length) {
				await parseUbicaciones(result.recordset);
			}

			offset = offset + nextrows;
			logger.info(
				`Ubicaciones: Procesando:${offset} filas de ${totalRows}`
			);
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateParte() {
	const schema = "ParteDiario";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		let offset = 0;
		const nextrows = 1000;
		const totalRowsQuery = await request.query(
			"SELECT COUNT(*) total FROM Personal_PartesDiarios"
		);
		const totalRows = totalRowsQuery.recordset[0].total;
		while (totalRows >= offset) {
			const result = await request.query`
                SELECT
                    parte.id,
                    fecha,
                    idUbicacion,
                    idEstado,
                    estado.nombre estadoNombre,
                    procesado,
                    audit_user,
                    audit_fecha
                FROM Personal_PartesDiarios parte
                LEFT JOIN Personal_PartesDiarios_Estados estado ON (parte.idEstado = estado.id)
                -- WHERE idUbicacion = 352 Testing Gestion de Personal
                ORDER BY fecha DESC
                OFFSET ${offset} ROWS
                FETCH NEXT ${nextrows} ROWS ONLY`;
			if (result.recordset && result.recordset.length) {
				for (const row of result.recordset) {
					const parte = await parseParte(row);
					await migrateParteAgentes(parte);
				}
			}
			offset = offset + nextrows;
			logger.info(`Partes: Procesando:${offset} filas de ${totalRows}`);
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateParteAgentes(parte) {
	const schema = "ParteAgentes";
	try {
		const request = sqlDB.request();
		const result = await request.query`
                SELECT
                    parte.id,
                    idParte,
                    idAgente,
                    agente.Numero numeroAgente,
                    idOpcion,
                    jus.nombre justificacionNombre,
                    observacion
                FROM Personal_PartesDiarios_Agentes parte
                LEFT JOIN Personal_Agentes agente ON (parte.idAgente = agente.ID)
                LEFT JOIN Personal_PartesDiarios_Opciones jus ON (parte.idOpcion = jus.id)
                WHERE idParte = ${parte.idLegacy}`;
		if (result.recordset && result.recordset.length) {
			await parsePartesAgentes(result.recordset, parte);
		}
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateFichadasAgente(agente) {
	try {
		logger.debug(`Migrando fichadas del agente nro ${agente.numero}`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT
                idAgente,
                fecha,
                esEntrada,
                reloj,
                format,
                data1,
                data2
            FROM Personal_Fichadas
            WHERE idAgente = ${agente.idLegacy}
            ORDER BY fecha DESC`;
		if (result.recordset[0]) {
			await parseFichadasAgente(result.recordset, agente);
		}
	} catch (err) {
		logger.error(err);
	}
}

async function migrateFichadasCache(agente) {
	try {
		logger.debug(`Migrando fichadas cache del agente nro ${agente.numero}`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT
                idAgente,
                fecha,
                entrada,
                salida
            FROM Personal_Fichadas_Cache
            WHERE idAgente = ${agente.idLegacy}
            ORDER BY fecha DESC`;
		if (result.recordset[0]) {
			await parseFichadasCacheAgente(result.recordset, agente);
		}
	} catch (err) {
		logger.error(err);
	}
}

/**
 * Deprecated
 * @param {*} row
 * @param {*} agente
 */
async function parseFichadaAgente(row, agente) {
	let fichada = {
		agente: { _id: agente._id },
		fecha: row.fecha,
		esEntrada: row.esEntrada,
		reloj: row.reloj,
	};
	return await api.addData("fichada", fichada);
}

/**
 * Deprecated
 * @param {*} row
 * @param {*} agente
 */
async function parseFichadaCache(row, agente) {
	let fichadaCache = {
		agente: { _id: agente._id },
		fecha: row.fecha,
		entrada: row.entrada,
		salida: row.salida,
	};
	return await api.addData("fichadaCache", fichadaCache);
}

async function parseFichadasAgente(rows, agente) {
	let fichadas = [];
	for (const row of rows) {
		let fichada = {
			agente: { _id: agente._id },
			fecha: row.fecha,
			esEntrada: row.esEntrada,
			reloj: row.reloj,
		};
		fichadas.push(fichada);
	}
	return await controller.addFichadasAgente(agente, fichadas);
}

async function parseFichadasCacheAgente(rows, agente) {
	let fichadasCache = [];
	for (const row of rows) {
		let fichadaCache = {
			agente: { _id: agente._id },
			fecha: row.fecha,
			entrada: row.entrada,
			salida: row.salida,
		};
		fichadasCache.push(fichadaCache);
	}
	return await controller.addFichadasCacheAgente(agente, fichadasCache);
}

async function parseParteEstado(row) {
	let codigo = 0; // Sin presentar
	switch (row.nombre) {
		case "Presentación parcial":
			codigo = 1;
			break;
		case "Presentación total":
			codigo = 2;
			break;
	}

	return await utils.parseObject(
		"parteEstado",
		{ nombre: row.nombre },
		{
			nombre: row.nombre,
			codigo: codigo,
		}
	);
}

async function parseParteJustificacion(row) {
	return await utils.parseObject(
		"parteJustificacion",
		{ nombre: row.nombre },
		{
			nombre: row.nombre,
			hint: row.hint,
		}
	);
}

async function parseUbicacionTipo(row) {
	return await utils.parseObject(
		"ubicacionTipo",
		{ codigo: row.Codigo },
		{
			codigo: row.Codigo,
			nombre: row.Nombre,
		}
	);
}

async function parseUbicacionSubtipo(row) {
	return await utils.parseObject(
		"ubicacionSubtipo",
		{ codigo: row.Codigo },
		{
			codigo: row.Codigo,
			nombre: row.Nombre,
		}
	);
}

/**
 * Deprecated
 * @param {*} row
 */
async function parseUbicacion(row) {
	let ancestors = await findUbicacionAncestors(row.Codigo);
	return await utils.parseObject(
		"ubicacion",
		{ codigo: row.Codigo },
		{
			padre: row.Padre,
			codigo: row.Codigo,
			nombre: row.Nombre,
			nombreCorto: row.NombreCorto,
			interno: row.Interno,
			tipo: row.Tipo,
			subtipo: row.Subtipo,
			telefono: row.Telefono,
			despachoHabilitado: row.Despacho_Habilitado,
			ancestors: ancestors,
		}
	);
}

async function parseUbicaciones(rows) {
	let ubicaciones = [];
	for (const row of rows) {
		let ancestors = await findUbicacionAncestors(row.Codigo);
		let ubi = {
			padre: row.Padre,
			codigo: row.Codigo,
			nombre: row.Nombre,
			nombreCorto: row.NombreCorto,
			interno: row.Interno,
			tipo: row.Tipo,
			subtipo: row.Subtipo,
			telefono: row.Telefono,
			despachoHabilitado: row.Despacho_Habilitado,
			ancestors: ancestors,
		};
		ubicaciones.push(ubi);
	}
	await controller.addUbicaciones(ubicaciones);
}

async function parseParte(row) {
	let ubicacion = await controller.getUbicacionByCodigo(row.idUbicacion);
	let estado = await controller.getParteEstadoByNombre(row.estadoNombre);
	let parte = {
		idLegacy: row.id,
		fecha: row.fecha,
		ubicacion: { codigo: ubicacion.codigo, nombre: ubicacion.nombre },
		estado: { _id: estado._id, nombre: estado.nombre },
		procesado: row.procesado,
		audit_user: row.audit_user,
		audit_fecha: row.audit_fecha,
	};
	return await controller.addParte(parte);
}

/**
 * Deprecated
 * @param {*} row
 * @param {*} parte
 */
async function parseParteAgente(row, parte) {
	let agentes = await api.getData("agente", { numero: row.numeroAgente });
	let agente = agentes[0];
	if (agente) {
		let justificacion = await utils.parseObject("parteJustificacion", {
			nombre: row.justificacionNombre,
		});

		let parteAgente = {
			parte: { _id: parte._id },
			fecha: parte.fecha,
			agente: {
				_id: agente._id,
				nombre: agente.nombre,
				apellido: agente.apellido,
			},
			justificacion: justificacion,
			observaciones: row.observacion,
		};
		return await api.addData("parteAgente", parteAgente);
	}
}

async function parsePartesAgentes(rows, parte) {
	let partesAgentes = [];
	for (const row of rows) {
		let agente = await controller.getAgenteByNumero(row.numeroAgente);
		if (agente) {
			let justificacion = await controller.getParteJustificacionByNombre(
				row.justificacionNombre
			);
			let parteAgente = {
				parte: { _id: parte._id },
				fecha: parte.fecha,
				agente: {
					_id: agente._id,
					nombre: agente.nombre,
					apellido: agente.apellido,
				},
				justificacion: justificacion,
				observaciones: row.observacion,
			};
			partesAgentes.push(parteAgente);
		}
	}
	return await controller.addPartesAgentes(parte, partesAgentes);
}

module.exports = {
	migrateAuxiliarTables: migrateAuxiliarTables,
	migrateFichadasAgente: migrateFichadasAgente,
	migrateFichadasCache: migrateFichadasCache,
	migrateParte: migrateParte,
};
