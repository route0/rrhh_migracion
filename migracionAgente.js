const logger = require("./logger").logger;
const { sqlDB } = require("./db");
const api = require("./api");
const historiaLaboral = require("./migracionCargoAgente");
const ausentismo = require("./migracionAusencias");
const bajasAgente = require("./migracionBajaAgente");
const partes = require("./migracionPartes");
const utils = require("./utils");

const tiposEstadoCivil = {
	"SEPARADO/A": "separado",
	"CASADO/A": "casado",
	"SOLTERO/A": "soltero",
	"VIUDO/A": "viudo",
};

const tiposSexo = {
	FEMENINO: "femenino",
	MASCULINO: "masculino",
};

const nacionalidades = {
	ARGENTINA: { nombre: "Argentina", gentilicio: "Argentina" },
	"ARGENTINO NATURALIZADO": { nombre: "Argentina", gentilicio: "Argentina" },
	CHILENA: { nombre: "Chile", gentilicio: "Chilena" },
	BRASILERA: { nombre: "Brasil", gentilicio: "Brasilera" },
	BOLIVIANA: { nombre: "Bolivia", gentilicio: "Boliviana" },
	ESPAÑOLA: { nombre: "España", gentilicio: "Española" },
	CANADIENSE: { nombre: "Canadá", gentilicio: "Canadiense" },
	OTRAS: null,
};

const provincias = {
	MENDOZA: "Mendoza",
	NEUQUÉN: "Neuquén",
	CHUBUT: "Chubut",
	CORDOBA: "Córdoba",
	"RIO NEGRO": "Río Negro",
	"BUENOS AIRES": "Buenos Aires",
	"(PROVINCIA FUERA DE LA ARGENTINA)": null,
};

const localidades = {
	"00000": { nombre: "FUERA DE NQN" },
	"0001": { nombre: "CIPOLLETTI", provincia: "RIO NEGRO" },
	"0002": { nombre: "S/D", provincia: "RIO NEGRO" },
	"0003": { nombre: "VILLA EL CHOCON", provincia: "NEUQUÉN" },
	"00701": { nombre: "ALUMINE", provincia: "NEUQUÉN" },
	"01407": { nombre: "SAN PATRICIO DEL CHAÑAR-AÑELO", provincia: "NEUQUÉN" },
	"03503": { nombre: "CENTENARIO", provincia: "NEUQUÉN" },
	"03504": {
		nombre: "COLONIA VALENTINA",
		altCode: "03507",
		provincia: "NEUQUÉN",
	},
	"03505": { nombre: "CUTRAL CO", provincia: "NEUQUÉN" },
	"03507": { nombre: "NEUQUEN", provincia: "NEUQUÉN" },
	"03508": { nombre: "PLAZA HUINCUL", provincia: "NEUQUÉN" },
	"03509": { nombre: "PLOTTIER", provincia: "NEUQUÉN" },
	"03510": { nombre: "SENILLOSA", provincia: "NEUQUÉN" },
	"03519": { nombre: "EL CHOCON", altCode: "0003", provincia: "NEUQUÉN" },
	"03535": {
		nombre: "CIUDAD INDUSTRIAL",
		altCode: "03507",
		provincia: "NEUQUÉN",
	},
	"09107": {
		nombre: "RINCON DE LOS SAUCES-PEHUENCHES",
		provincia: "NEUQUÉN",
	},
	15000: { nombre: "CIPOLLETTI", altCode: "0001", provincia: "RIO NEGRO" },
	15001: { nombre: "ALLEN", provincia: "RIO NEGRO" },
	15002: { nombre: "GENERAL ROCA", provincia: "RIO NEGRO" },
	15003: { nombre: "CINCO SALTOS", provincia: "RIO NEGRO" },
	15004: { nombre: "FERNANDEZ ORO", provincia: "RIO NEGRO" },
	15008: { nombre: "S/D", altCode: "0002", provincia: "RIO NEGRO" },
	15009: { nombre: "PLOTTIER", altCode: "03509", provincia: "NEUQUÉN" },
	8315: { nombre: "PIEDRA DEL AGUILA", provincia: "NEUQUÉN" },
};

async function migrateAuxiliarTables() {
	await migrateLocalidades();
}

async function migrateLocalidades() {
	const schema = "Localidades";
	logger.debug(`Inicio migracion de ${schema}!!!`);
	const localidadesCode = Object.keys(localidades);
	for (const locaCode of localidadesCode) {
		let code = localidades[locaCode].altCode
			? localidades[locaCode].altCode
			: locaCode;

		await parseLocalidad(code);
	}
	logger.debug(`Fin migracion de ${schema}!!!`);
}

async function parseLocalidad(localidadCode) {
	if (!utils.isEmpty(localidadCode)) {
		const localidad = localidades[localidadCode];
		if (!localidad) return null;
		const provincia = await parseProvincia(localidad.provincia);
		return await utils.parseObject(
			"localidad",
			{ nombre: localidad.nombre },
			{
				nombre: localidad.nombre,
				provincia: provincia,
				codigo: localidadCode,
			}
		);
	} else {
		return null;
	}
}

async function parseProvincia(nombre) {
	if (nombre == "(provincia fuera de la Argentina)") return null;
	const pais = await parseNacionalidad("ARGENTINA");
	if (!utils.isEmpty(nombre)) {
		return await utils.parseObject(
			"provincia",
			{ nombre: nombre },
			{ nombre: nombre, pais: pais }
		);
	} else {
		return null;
	}
}

async function parseNacionalidad(nacionalidad) {
	let nacion = nacionalidad ? nacionalidades[nacionalidad] : null;
	if (nacion == null) return null;
	let pais = await api.getPaises(nacion.nombre);
	if (!pais.length) {
		pais = await api.addPais(nacion);
	} else {
		pais = pais[0];
	}
	return pais;
}

async function migrateAgentes() {
	logger.debug("Inicio proceso de Migracion de Agentes");
	try {
		const request = sqlDB.request();
		let offset = 0;
		const nextrows = 300;
		const totalRowsQuery = await request.query(
			"SELECT COUNT(*) total FROM Personal_Agentes"
		);
		const totalRows = totalRowsQuery.recordset[0].total;
		while (totalRows >= offset) {
			const result = await request.query`
                SELECT ID, Numero, Tipo_Documento,
                    LTRIM(RTRIM(Documento)) Documento,
                    LTRIM(RTRIM(CUIL)) CUIL,
                    LTRIM(RTRIM(Apellido)) Apellido,
                    LTRIM(RTRIM(agente.Nombre)) as NombreAgente,
                    UPPER(Estado_Civil) Estado_Civil, UPPER(Nacionalidad) Nacionalidad, UPPER(Sexo) Sexo,
                    Fecha_Nacimiento,
                    Direccion_Codigo_Postal,
                    UPPER(Direccion_Provincia) Direccion_Provincia,
                    
                    Direccion_Localidad LocalidadCodigo,
                    LTRIM(RTRIM(Direccion_Barrio)) Direccion_Barrio,
                    LTRIM(RTRIM(Direccion_Paraje)) Direccion_Paraje,
                    LTRIM(RTRIM(Direccion_Calle)) Direccion_Calle,
                    LTRIM(RTRIM(Direccion_Numero)) Direccion_Numero,
                    LTRIM(RTRIM(Direccion_Piso)) Direccion_Piso,
                    LTRIM(RTRIM(Direccion_Dpto)) Direccion_Dpto,
                    LTRIM(RTRIM(Direccion_CalleIzquierda)) Direccion_CalleIzquierda,
                    LTRIM(RTRIM(Direccion_CalleDerecha)) Direccion_CalleDerecha,
                    LTRIM(RTRIM(Direccion_CalleParalela)) Direccion_CalleParalela,
                    LTRIM(RTRIM(Direccion_Completarios)) Direccion_Completarios,

                    Telefono_Caracteristica, Telefono_Numero,
                    Telefono_CodigoCelular, Telefono_Celular,
                    EstudiosPrimario, EstudiosPrimario_Titulo,
                    EstudiosSecundarios, EstudiosSecundarios_Titulo,
                    EstudiosTerciarios, EstudiosTerciarios_Titulo,
                    EstudiosUniversitarios, EstudiosUniversitarios_Titulo,
                    EstudiosPostgrado, EstudiosPostgrado_Titulo,
                    EstudiosEspecialidad, EstudiosEspecialidad_Titulo,
                    EstudiosTituloAbreviado,
                    
                    Situacion_EnPlanta, Situacion_LugarPago,
                    Situacion_FechaIngresoEstado, Situacion_FechaIngresoHospital, 
                    Antiguedad_Vacaciones, Antiguedad_Pago,
                    exceptuadoFichado, trabajaEnHospital, Traslado_Desde,
                
                    Activo,
                    Foto, Foto_type,
                
                    TimeStamp,
                    codigoFichado
                FROM Personal_Agentes agente
                -- WHERE Documento IN ('26810671', '20465489', '20989272', '32577273', '691011', '39685', '10737', '151019', '11648') 
                -- WHERE Numero IN ('239316')
                ORDER BY ID
                OFFSET ${offset} ROWS
                FETCH NEXT ${nextrows} ROWS ONLY`;
			if (result.recordset && result.recordset.length) {
				for (const row of result.recordset) {
					try {
						let agente = await parseAgente(row);
						logger.debug(
							`Procesando Agente: ${agente.nombre}, ${agente.apellido}`
						);
						let historial = await historiaLaboral.migrateHistorialAgente(
							agente.numero
						);
						let situacionLaboral = await historiaLaboral.getSituacionActivaFromHistorial(
							row,
							agente,
							historial
						);
						if (situacionLaboral) {
							agente.situacionLaboral = situacionLaboral;
							agente.situacionLaboral.fecha = situacionLaboral.normaLegal
								? situacionLaboral.normaLegal.fechaNormaLegal
								: undefined;
						}

						// Las bajas y modificaciones en el historial forman parte de la
						// nueva historia laboral del agente, segun el nuevo esquema
						let nuevaHistoriaLaboral = [];
						let bajas = await bajasAgente.migrateBajasAgente(
							agente
						);
						for (const baja of bajas) {
							let nuevaHistoria = {
								tipo: "baja",
								timestamp: undefined,
								changeset: baja,
							};
							nuevaHistoriaLaboral.push(nuevaHistoria);
						}
						for (const cambio of historial) {
							let nuevaHistoria = {
								tipo: "modificacion",
								timestamp: undefined,
								changeset: cambio,
							};
							nuevaHistoriaLaboral.push(nuevaHistoria);
						}
						// Ordenamos la nueva historia laboral por fecha desc
						nuevaHistoriaLaboral.sort(
							(a, b) => b.changeset.fecha - a.changeset.fecha
						);
						agente.historiaLaboral = nuevaHistoriaLaboral;
						agente.migracion = true; // Para identificar el origen de la llamada
						agente = await api.saveAgente(agente);
						// let agente = await parseAgenteSimple(row); // Use for testing
						// logger.debug(`Procesando Agente: ${agente.nombre}, ${agente.apellido}`);

						await ausentismo.migrateAusenciasAgente(agente);
						await ausentismo.migrateIndicadoresAgente(agente);
						await ausentismo.migrateFrancosAgente(agente);
						await partes.migrateFichadasAgente(agente);
						await partes.migrateFichadasCache(agente);
					} catch (err) {
						logger.error(err);
					}
				}
			}

			offset = offset + nextrows;
			logger.info(`Procesando:${offset} filas`);
		}
	} catch (err) {
		logger.error(err);
	}
}

function parseDireccion(calle, numero, piso, dpto) {
	calle = calle ? calle : "";
	numero = numero ? numero : "";
	piso = piso ? `Piso ${piso}` : "";
	dpto = dpto ? `Dpto ${dpto}` : "";
	return `${calle} ${numero} ${piso} ${dpto}`.trim();
}

function parseImage(img) {
	return img;
}

function parseContactos(telCodigo, telNro, celCodigo, celNro) {
	let contactos = [];
	if (!utils.isEmpty(telNro)) {
		const caracteristica = telCodigo ? `(${telCodigo})-` : "";
		const telefono = { tipo: "fijo", valor: `${caracteristica}${telNro}` };
		contactos.push(telefono);
	}
	if (!utils.isEmpty(celNro)) {
		const caracteristica = celCodigo ? `(${celCodigo})-` : "";
		const celular = {
			tipo: "celular",
			valor: `${caracteristica}${celNro}`,
		};
		contactos.push(celular);
	}
	return contactos;
}

function tieneTitulo(estudio) {
	if (estudio == null || estudio == 0) {
		return false;
	} else {
		return true;
	}
}

async function parseInfoEducativa(
	EstudiosPrimario,
	EstudiosPrimario_Titulo,
	EstudiosSecundarios,
	EstudiosSecundarios_Titulo,
	EstudiosTerciarios,
	EstudiosTerciarios_Titulo,
	EstudiosUniversitarios,
	EstudiosUniversitarios_Titulo,
	EstudiosPostgrado,
	EstudiosPostgrado_Titulo,
	EstudiosEspecialidad,
	EstudiosEspecialidad_Titulo
) {
	let educacion = [];
	if (tieneTitulo(EstudiosPrimario)) {
		const titulo = await parseEducacion(
			"primario",
			EstudiosPrimario_Titulo
		);
		educacion.push(titulo);
	}
	if (tieneTitulo(EstudiosSecundarios)) {
		const titulo = await parseEducacion(
			"secundario",
			EstudiosSecundarios_Titulo
		);
		educacion.push(titulo);
	}
	if (tieneTitulo(EstudiosTerciarios)) {
		const titulo = await parseEducacion(
			"terciario",
			EstudiosTerciarios_Titulo
		);
		educacion.push(titulo);
	}
	if (tieneTitulo(EstudiosUniversitarios)) {
		const titulo = await parseEducacion(
			"universitario",
			EstudiosUniversitarios_Titulo
		);
		educacion.push(titulo);
	}
	if (tieneTitulo(EstudiosPostgrado)) {
		const titulo = await parseEducacion(
			"postgrado",
			EstudiosPostgrado_Titulo
		);
		educacion.push(titulo);
	}
	if (tieneTitulo(EstudiosEspecialidad)) {
		const titulo = await parseEducacion(
			"especialidad",
			EstudiosEspecialidad_Titulo
		);
		educacion.push(titulo);
	}
	return educacion;
}

async function parseEducacion(tipoEducacion, titulo) {
	let titulos = await api.getEducacion(titulo);
	if (!titulos.length) {
		educacion = await api.addEducacion({
			tipoEducacion: tipoEducacion,
			titulo: titulo,
		});
	} else {
		educacion = titulos[0];
	}
	return educacion;
}

async function parseAgenteSimple(row) {
	let agente = {
		idLegacy: row.ID,
		numero: row.Numero,
		nombre: row.NombreAgente,
		apellido: row.Apellido,
	};
	return agente;
}

async function parseAgente(row) {
	let agente = {
		idLegacy: row.ID,
		numero: row.Numero,
		tipoDocumento: row.Tipo_Documento,
		documento: row.Documento,
		cuil: row.CUIL ? row.CUIL : null,
		nombre: row.NombreAgente,
		apellido: row.Apellido,
		estadoCivil: row.Estado_Civil
			? tiposEstadoCivil[row.Estado_Civil]
			: null,
		sexo: tiposSexo[row.Sexo],
		genero: tiposSexo[row.Sexo],
		fechaNacimiento: utils.parseDate(row.Fecha_Nacimiento),
		nacionalidad: await parseNacionalidad(row.Nacionalidad),
		direccion: {
			valor: parseDireccion(
				row.Direccion_Calle,
				row.Direccion_Numero,
				row.Direccion_Piso,
				row.Direccion_Dpto
			),
			barrio: row.Direccion_Barrio,
			localidad: await parseLocalidad(row.LocalidadCodigo),
			codigoPostal: row.Direccion_Codigo_Postal,
			calleIzquierda: row.Direccion_CalleIzquierda,
			calleDerecha: row.Direccion_CalleDerecha,
			calleParalela: row.Direccion_CalleParalela,
			complementarios: `${row.Direccion_Completarios}`, // TODO Que hacer con ${row.Direccion_Paraje}
		},
		contactos: parseContactos(
			row.Telefono_Caracteristica,
			row.Telefono_Numero,
			row.Telefono_CodigoCelular,
			row.Telefono_Celular
		),
		educacion: await parseInfoEducativa(
			row.EstudiosPrimario,
			row.EstudiosPrimario_Titulo,
			row.EstudiosSecundarios,
			row.EstudiosSecundarios_Titulo,
			row.EstudiosTerciarios,
			row.EstudiosTerciarios_Titulo,
			row.EstudiosUniversitarios,
			row.EstudiosUniversitarios_Titulo,
			row.EstudiosPostgrado,
			row.EstudiosPostgrado_Titulo,
			row.EstudiosEspecialidad,
			row.EstudiosEspecialidad_Titulo
		),

		foto: row.Foto ? parseImage(row.Foto) : null,
		activo: row.Activo,
	};
	return agente;
}

module.exports = {
	migrateAuxiliarTables: migrateAuxiliarTables,
	migrateAgentes: migrateAgentes,
};
