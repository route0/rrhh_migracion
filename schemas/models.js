const { Schema, Types, model } = require("mongoose");

// const audit = require('../../../packages/mongoose-audit-trail');

const UbicacionSchema = new Schema({
	padre: {
		type: Number, // Self reference to codigo field
		index: true,
	},
	codigo: {
		type: Number,
		index: true,
		required: true,
	},
	nombre: {
		type: String,
		index: true,
		required: true,
	},
	nombreCorto: String,
	interno: Boolean,
	tipo: {
		type: Number,
		index: true,
		required: true,
	},
	subtipo: Number,
	telefono: String,
	despachoHabilitado: Boolean,
	ancestors: [Number], // Para simplificar las consultas. See https://docs.mongodb.com/manual/tutorial/model-tree-structures-with-ancestors-array/
});

const ParteEstadoSchema = new Schema({
	nombre: {
		type: String,
		required: true,
	},
	codigo: Number,
});

const ParteJustificacionSchema = new Schema({
	nombre: {
		type: String,
		required: true,
	},
	hint: String,
});

const ParteSchema = new Schema({
	idLegacy: Number, // ID Sistema anterior.
	fecha: {
		type: Date,
		required: true,
		index: true,
	},
	ubicacion: {
		// _id: {
		//     type: Types.ObjectId,
		//     required: true
		// },
		codigo: {
			type: Number,
			required: true,
		},
		nombre: String,
	},
	estado: {
		_id: {
			type: Types.ObjectId,
			required: true,
		},
		nombre: String,
	},
	procesado: Boolean,
	novedades: Boolean, // Flag. True si al menos uno de los partes de los agentes tiene novedades
	editadoPostProcesado: Boolean,
	fechaEnvio: Date,
	usuarioEnvio: {
		// TODO. Consultar: El usuario es siempre un agente?
		_id: {
			type: Types.ObjectId,
			// required: true,
			index: true,
		},
		nombre: String,
		apellido: String,
	},
	audit_user: Number,
	audit_fecha: Date,
});

// ParteSchema.plugin(audit.plugin, { omit: ["_id", "id"] })

const ParteAgenteSchema = new Schema({
	parte: {
		_id: {
			type: Types.ObjectId,
			required: true,
			index: true,
		},
	},
	agente: {
		_id: {
			type: Types.ObjectId,
			required: true,
			index: true,
		},
		nombre: String,
		apellido: String,
	},
	fecha: {
		type: Date,
		index: true,
	},
	fichadas: {
		entrada: Date,
		salida: Date,
		horasTrabajadas: String,
	},
	ausencia: {
		articulo: {
			_id: Types.ObjectId,
			codigo: String,
			descripcion: String,
		},
	},
	justificacion: {
		_id: Types.ObjectId,
		nombre: String,
	},
	observaciones: String,
});

// ParteAgenteSchema.plugin(audit.plugin, { omit: ["_id", "id"] })

const FichadaSchema = new Schema({
	agente: {
		_id: {
			type: Types.ObjectId,
			required: true,
			index: true,
		},
		nombre: String,
		apellido: String,
	},
	fecha: {
		type: Date,
		required: true,
		index: true,
	},
	esEntrada: Boolean,
	reloj: Number,
});

const FichadaCacheSchema = new Schema({
	agente: {
		_id: {
			type: Types.ObjectId,
			required: true,
			index: true,
		},
		nombre: String,
		apellido: String,
	},
	fecha: {
		type: Date,
		required: true,
		index: true,
	},
	entrada: Date,
	salida: Date,
	horasTrabajadas: String,
});

const AgenteSchema = new Schema({
	idLegacy: Number, // ID Sistema anterior.
	numero: String, // En el alta aun no esta disponible este dato
	tipoDocumento: String, // No deberia utilizarse mas. Solo DU
	documento: {
		type: String,
		required: true,
		es_indexed: true,
	},
	cuil: {
		type: String,
		required: false, // No todos los agentes tienen CUIL en SQLServer
		es_indexed: true,
	},
	nombre: {
		type: String,
		required: true,
		es_indexed: true,
	},
	apellido: {
		type: String,
		required: true,
		es_indexed: true,
	},
	// estadoCivil: constantes.ESTADOCIVIL,
	// sexo: constantes.SEXO,
	// genero: constantes.GENERO,
	// fechaNacimiento: {
	// 	type: Date,
	// 	set: parseDate,
	// 	get: parseDate,
	// 	es_indexed: true,
	// },
	// nacionalidad: PaisSchema,
	// direccion: DireccionSchema,
	// contactos: [ContactoSchema],
	// educacion: [EducacionSchema],
	// especialidad: EspecialidadSchema, // TODO Ver especialidadSchema
	// situacionLaboral: SituacionLaboralSchema,
	// historiaLaboral: [HistoriaLaboralSchema],
	activo: Boolean,
});

const ArticuloSchema = new Schema({
	idInterno: {
		// Codigo Interno Sistema Anterior. TODO Remover si no es mas necesario
		type: Number,
	},
	codigo: {
		// Codigo alfanumerico
		type: String,
		required: true,
	},
	nombre: {
		// Nombre del articulo
		type: String,
		required: true,
	},
	descripcion: String,
	color: String, // A visualizar cuando sea necesario. Verde claro=#5cb85c, rojo= #d9534f
	diasCorridos: {
		// Indica si los dias a ausencia son corridos
		type: Boolean,
		default: true,
	},
	diasHabiles: {
		// Indica si los dias a ausencia son habiles
		type: Boolean,
		default: false,
	},
	descuentaDiasLicencia: {
		// Para uso de articulos especiales que descuentan dias de licencia
		type: Boolean,
		default: false,
	},
});

const FrancoSchema = new Schema({
	agente: {
		_id: {
			type: Types.ObjectId,
			required: true,
		},
	},
	fecha: {
		type: Date,
		required: true,
	},
});

// ArticuloSchema.plugin(audit.plugin, { omit: ["_id", "id"] });

const Agente = model("Agente", AgenteSchema, "agentes");
const Ubicacion = model("Ubicacion", UbicacionSchema, "ubicaciones");
const ParteEstado = model("ParteEstado", ParteEstadoSchema, "parteestados");
const ParteJustificacion = model(
	"ParteJustificacion",
	ParteJustificacionSchema,
	"partejustificaciones"
);
const Parte = model("Parte", ParteSchema, "partes");
const ParteAgente = model("ParteAgente", ParteAgenteSchema, "partesagentes");
const Fichada = model("Fichada", FichadaSchema, "fichadas");
const FichadaCache = model("FichadaCache", FichadaCacheSchema, "fichadascache");
const Articulo = model("Articulo", ArticuloSchema, "articulos");
const Franco = model("Franco", FrancoSchema, "francos");

module.exports = {
	Agente: Agente,
	Ubicacion: Ubicacion,
	ParteEstado: ParteEstado,
	ParteJustificacion: ParteJustificacion,
	Parte: Parte,
	ParteAgente: ParteAgente,
	Fichada: Fichada,
	FichadaCache: FichadaCache,
	Articulo: Articulo,
	Franco: Franco,
};
