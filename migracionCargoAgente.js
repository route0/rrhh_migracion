const { sqlDB } = require("./db");
const api = require("./api");
const utils = require("./utils");
const logger = require("./logger").logger;

async function migrateAuxiliarTables() {
	await migrateTipoSituacion();
	await migrateDirecciones();
	await migrateDepartamentos();
	await migrateSectores();
	await migrateServicios();
	await migrateSubpuestos();
}

async function migrateHistorialAgente(agenteID) {
	var historiaLaboral = [];
	try {
		const request = sqlDB.request();
		const result = await request.query`
            SELECT Tipo,
                    Decreto,
                    Agente,
                    Fecha,
                    Categoria,
                    Agrupamiento,
                    Puesto_trabajo,
                    Subpuesto_trabajo,
                    Lugar_trabajo,
                    sector.Sector as Sector,
                    sector.Jefe as SectorJefe,
                    sector.Servicio as SectorServicio,
                    sector.Ubicacion as SectorUbicacion,
                    sector.NombreViejo as SectorNombreViejo,
                    servicio.Servicio as Servicio,
                    servicio.Jefe as ServicioJefe,
                    servicio.Departamento as ServicioDepartamento,
                    servicio.Ubicacion as ServicioUbicacion,
                    servicio.id as ServicioCodigo,
                    servicio.NombreViejo as ServicioNombreViejo,
                    departamento.Departamento as Departamento, -- Redundante
                    departamento.Jefe as DepartamentoJefe,
                    departamento.Dirección as DepartamentoDireccion,
                    departamento.Ubicacion as DepartamentoUbicacion,
                    direccion.Direccion as Direccion, -- Redundante
                    direccion.[Jefe Direccion] as DireccionJefe,
                    direccion.Ubicacion as DireccionUbicacion,
                    Regimen_Horario,
                    Prolongacion_Jornada,
                    Actividad_Critica,
                    Tiempo_Pleno,
                    Dedicacion_Exclusiva,
                    Guardias_Pasivas,
                    Guardias_Activas,
                    Observaciones,
                    Inactivo
            FROM Personal_Historial_Laboral_Agentes cargo
            LEFT JOIN Personal_Sectores sector ON (cargo.Lugar_trabajo = sector.Sector)
            LEFT JOIN Personal_Servicios servicio ON (cargo.Servicio = servicio.Servicio)
            LEFT JOIN Personal_Departamentos departamento ON (servicio.Departamento = departamento.Departamento)
            LEFT JOIN Personal_Direcciones direccion ON (departamento.Dirección = direccion.Direccion)
            WHERE Agente = ${agenteID}
            ORDER BY Fecha ASC`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				const historia = await parseHistoriaLaboral(row);
				historiaLaboral.push(historia);
			}
		}
		historiaLaboral.sort((a, b) => b.fecha - a.fecha);
		return historiaLaboral;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateTipoSituacion() {
	const schema = "TipoSituacion";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT [Situacion en Planta] as Situacion
            FROM Personal_Situaciones_en_Planta`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseTipoSituacion(row);
			}
		}
		// Debemos crear un nuevo tipo antes de finalizar
		await parseTipoSituacion({ Situacion: "Trabajador Eventual" });
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateSectores() {
	const schema = "Sectores";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                Sector,
                Servicio as SectorServicio,
                Jefe as SectorJefe,
                Ubicacion as SectorUbicacion,
                NombreViejo as SectorViejo
            FROM Personal_Sectores`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseSector(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateDirecciones() {
	const schema = "Direcciones";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                Direccion,
                [Jefe Direccion] as DireccionJefe,
                Ubicacion as DireccionUbicacion
            FROM Personal_Direcciones`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseDireccion(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateDepartamentos() {
	const schema = "Departamentos";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                departamento.Departamento as Departamento,
                departamento.Jefe as DepartamentoJefe,
                departamento.Dirección,
                departamento.Ubicacion as DepartamentoUbicacion,
                direccion.[Jefe Direccion] as DireccionJefe,
                direccion.Direccion as Direccion,
                direccion.Ubicacion as DireccionUbicacion
            FROM Personal_Departamentos departamento
            LEFT JOIN Personal_Direcciones direccion ON (departamento.Dirección = direccion.Direccion)
            `;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseDepartamento(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateServicios() {
	const schema = "Servicios";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                servicio.Servicio as Servicio,
                servicio.Jefe as ServicioJefe,
                servicio.Departamento as Departamento,
                servicio.Ubicacion as ServicioUbicacion,
                servicio.id as ServicioCodigo,
                servicio.NombreViejo as ServicioNombreViejo,
                departamento.Jefe as DepartamentoJefe,
                departamento.Ubicacion as DepartamentoUbicacion,
                direccion.Direccion as Direccion,
                direccion.[Jefe Direccion] as DireccionJefe,
                direccion.Ubicacion as DireccionUbicacion
            FROM Personal_Servicios servicio
            LEFT JOIN Personal_Departamentos departamento ON (servicio.Departamento = departamento.Departamento)
            LEFT JOIN Personal_Direcciones direccion ON (departamento.Dirección = direccion.Direccion)`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseServicio(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function migrateSubpuestos() {
	const schema = "Subpuestos";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                Subpuesto
            FROM Personal_Subpuestos_de_trabajo`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseSubPuestoTrabajo(row.Subpuesto);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(`Error migracion de ${schema}!!!`);
		logger.error(err);
	}
}

async function parseTipoNormaLegal(nombre) {
	if (utils.isEmpty(nombre)) return null;
	let tiposNormas = await api.getTiposNormaLegal(nombre);
	if (!tiposNormas.length) {
		tipoNorma = await api.addTipoNormalLegal({ nombre: nombre });
	} else {
		tipoNorma = tiposNormas[0];
	}
	return tipoNorma;
}

async function parseDireccion(row) {
	return await utils.parseObject(
		"direccionCargo",
		{ nombre: row.Direccion },
		{
			nombre: row.Direccion,
			jefe: row.DireccionJefe,
			ubicacion: row.DireccionUbicacion,
		}
	);
}

async function parseDepartamento(row) {
	let direccion = await parseDireccion(row);
	return await utils.parseObject(
		"departamentoCargo",
		{ nombre: row.Departamento },
		{
			nombre: row.Departamento,
			jefe: row.DepartamentoJefe,
			direccion: direccion,
			ubicacion: row.DepartamentoUbicacion,
		}
	);
}

async function parseServicio(row) {
	let departamento = await parseDepartamento(row);
	return await utils.parseObject(
		"servicioCargo",
		{ nombre: row.Servicio },
		{
			nombre: row.Servicio,
			jefe: row.ServicioJefe,
			departamento: departamento,
			ubicacion: row.ServicioUbicacion,
			codigo: row.ServicioCodigo,
			nombreViejo: row.ServicioNombreViejo,
		}
	);
}

async function parseRegimen(nombre) {
	if (!utils.isEmpty(nombre)) {
		return await utils.parseObject(
			"regimenHorario",
			{ nombre: nombre },
			{ nombre: nombre }
		);
	} else {
		return null;
	}
}

async function parseAgrupamiento(nombre) {
	if (!utils.isEmpty(nombre)) {
		return await utils.parseObject(
			"agrupamiento",
			{ nombre: nombre },
			{ nombre: nombre }
		);
	} else {
		return null;
	}
}

async function parsePuestoTrabajo(nombre) {
	if (!utils.isEmpty(nombre)) {
		return await utils.parseObject(
			"puesto",
			{ nombre: nombre },
			{ nombre: nombre }
		);
	} else {
		return null;
	}
}

async function parseSubPuestoTrabajo(nombre) {
	if (!utils.isEmpty(nombre)) {
		return await utils.parseObject(
			"subpuesto",
			{ nombre: nombre },
			{ nombre: nombre }
		);
	} else {
		return null;
	}
}

async function parseSector(row) {
	return await utils.parseObject(
		"sector",
		{ nombre: row.Sector },
		{
			nombre: row.Sector,
			jefe: row.SectorJefe,
			servicio: row.SectorServicio,
			ubicacion: row.SectorUbicacion,
			nombreViejo: row.SectorNombreViejo,
		}
	);
}

async function parseRegimenAgente(row) {
	let regimen = {
		regimenHorario: await parseRegimen(row.Regimen_Horario),
		prolongacionJornada: row.Prolongacion_Jornada
			? row.Prolongacion_Jornada
			: "",
		actividadCritica: row.Actividad_Critica ? row.Actividad_Critica : "",
		tiempoPleno: row.Tiempo_Pleno ? row.Tiempo_Pleno : false,
		dedicacionExclusiva: row.Dedicacion_Exclusiva
			? row.Dedicacion_Exclusiva
			: false,
		guardiasPasivas: row.Guardias_Pasivas ? row.Guardias_Pasivas : false,
		guardiasActivas: row.Guardias_Activas ? row.Guardias_Activas : false,
	};
	return regimen;
}

async function parseNormaLegalAgente(row) {
	let tipoNormaLegal = await parseTipoNormaLegal(row.Tipo);
	let normaLegal = {
		tipoNormaLegal: tipoNormaLegal,
		numeroNormaLegal: row.Decreto,
		fechaNormaLegal: utils.parseDate(row.Fecha),
		observaciones: row.Observaciones ? row.Observaciones : "",
	};
	return normaLegal;
}

async function parseCargoAgente(row) {
	let servicioCargo = await parseServicio(row);
	let ubicacion = await utils.parseObject("ubicacion", {
		codigo: row.ServicioUbicacion,
	});
	let cargo = {
		agrupamiento: await parseAgrupamiento(row.Agrupamiento),
		puesto: await parsePuestoTrabajo(row.Puesto_trabajo),
		subpuesto: await parseSubPuestoTrabajo(row.Subpuesto_trabajo),
		sector: await parseSector(row),
		servicio: servicioCargo,
		ubicacion: ubicacion,
	};
	return cargo;
}

async function parseHistoriaLaboral(row) {
	let normaLegal = await parseNormaLegalAgente(row);
	let historia = {
		inactivo: row.Inactivo,
		normaLegal: normaLegal,
		cargo: await parseCargoAgente(row),
		regimen: await parseRegimenAgente(row),
		motivo: undefined,
		fecha: normaLegal ? normaLegal.fechaNormaLegal : undefined,
	};
	return historia;
}

async function getSituacionActivaFromHistorial(row, agente, historial) {
	let ultimaSituacion = null;
	const index = historial.findIndex((h) => !h.inactivo);
	if (index != -1) {
		if (agente.activo) {
			// Si el agente esta activo quitamos la situacion
			// del historial
			ultimaSituacion = historial.splice(index, 1)[0];
		} else {
			ultimaSituacion = historial[index];
		}
	}
	let situacionLaboral = await parseSituacionLaboral(row, ultimaSituacion);
	return situacionLaboral;
}

async function parseSituacionLaboral(row, situacion) {
	let agenteSituacionLaboral = await parseAgenteSituacionLaboral(row);
	let agenteTipoSituacion = await parseAgenteSituacionAgente(row);
	if (situacion) {
		agenteSituacionLaboral.normaLegal = situacion.normaLegal;
		agenteSituacionLaboral.cargo = situacion.cargo;
		agenteSituacionLaboral.regimen = situacion.regimen;
		agenteSituacionLaboral.situacion = agenteTipoSituacion;
	}
	return agenteSituacionLaboral;
}

async function parseAgenteSituacionLaboral(row) {
	return await parseSituacionLaboralDatosGenerales(
		row.Situacion_FechaIngresoEstado,
		row.Situacion_FechaIngresoHospital,
		row.Antiguedad_Vacaciones,
		row.Antiguedad_Pago
	);
}

async function parseSituacionLaboralDatosGenerales(
	Situacion_FechaIngresoEstado,
	Situacion_FechaIngresoHospital,
	Antiguedad_Vacaciones,
	Antiguedad_Pago
) {
	return {
		fechaIngresoEstado: utils.parseDate(Situacion_FechaIngresoEstado),
		fechaIngresoHospital: utils.parseDate(Situacion_FechaIngresoHospital),
		antiguedadVacaciones: utils.parseDate(Antiguedad_Vacaciones),
		antiguedadPago: utils.parseDate(Antiguedad_Pago),
		// El cargo y regimen se obtienen luego de la historia laboral
	};
}

async function parseAgenteSituacionAgente(row) {
	return await parseSituacionLaboralDatosSituacion(
		row.Situacion_EnPlanta,
		row.Situacion_LugarPago,
		row.exceptuadoFichado,
		row.trabajaEnHospital,
		row.Traslado_Desde
	);
}

async function parseSituacionLaboralDatosSituacion(
	Situacion_EnPlanta,
	Situacion_LugarPago,
	exceptuadoFichado,
	trabajaEnHospital,
	Traslado_Desde
) {
	return {
		exceptuadoFichado: exceptuadoFichado,
		trabajaEnHospital: trabajaEnHospital,
		trasladoDesde: Traslado_Desde ? Traslado_Desde : "",
		lugarPago: Situacion_LugarPago ? Situacion_LugarPago : "",
		tipoSituacion: Situacion_EnPlanta
			? await parseTipoSituacion({ Situacion: Situacion_EnPlanta })
			: null,
		// El cargo y regimen se obtienen luego de la historia laboral
	};
}

async function parseTipoSituacion(row) {
	const requiereVencimiento = [
		"Pasante",
		"Temporario",
		"Mensualizado",
		"Guardia reemplazo",
		"Trabajador Eventual",
		"Plan Invierno",
	].includes(row.Situacion);
	const activo = !["Mensualizado"].includes(row.Situacion);
	return await utils.parseObject(
		"tipoSituacion",
		{ nombre: row.Situacion },
		{
			nombre: row.Situacion,
			requiereVencimiento: requiereVencimiento,
			activo: activo,
		}
	);
}

module.exports = {
	migrateAuxiliarTables: migrateAuxiliarTables,
	migrateHistorialAgente: migrateHistorialAgente,
	getSituacionActivaFromHistorial: getSituacionActivaFromHistorial,
};
