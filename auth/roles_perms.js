const permisos_all = [
	"agentes:agente:add_agente",
	"agentes:agente:view_agente",
	"agentes:agente:change_agente",
	"agentes:agente:baja_agente",
	"agentes:agente:reactivar_agente",
	"agentes:agente:view_fichado",
	"agentes:agente:habilitar_fichado",
	"agentes:agente:inhabilitar_fichado",
	"agentes:agente:print_credencial",

	"agentes:historia:add_historialaboral",
	"agentes:historia:view_historialaboral",
	"agentes:historia:change_historialaboral",
	"agentes:historia:delete_historialaboral",

	"agentes:nota:add_nota",
	"agentes:nota:view_nota",
	"agentes:nota:change_nota",
	"agentes:nota:delete_nota",

	"agentes:ausentismo:add_ausentismo",
	"agentes:ausentismo:view_ausentismo",
	"agentes:ausentismo:change_ausentismo",
	"agentes:ausentismo:delete_ausentismo",

	"partes:parte:view_parte",
	"partes:parte:add_parte",
	"partes:parte:change_parte",
	"partes:parte:confirmar_parte",
	"partes:parte:procesar_parte",
	"partes:reporte:fichadas",
	"partes:reporte:partes",

	"reportes:agente:legajo_agentes",
	"reportes:agente:listado_agentes",
	"reportes:ausentismo:ausentismo",
	"reportes:ausentismo:ausentismo_totales",
	"reportes:ausentismo:licencias",

	"seguridad:usuario:view_usuario",
	"seguridad:usuario:change_usuario",
	"seguridad:usuario:add_usuario",
	"seguridad:usuario:delete_usuario",

	"seguridad:rol:view_rol",
	"seguridad:rol:change_rol",
	"seguridad:rol:add_rol",
	"seguridad:rol:delete_rol",

	"config:articulo:view_articulo",
	"config:articulo:change_articulo",
	"config:articulo:add_articulo",
	"config:articulo:delete_articulo",

	"config:feriado:view_feriado",
	"config:feriado:change_feriado",
	"config:feriado:add_feriado",
	"config:feriado:delete_feriado",

	"config:licencia:view_licencia",
	"config:licencia:change_licencia",
	"config:licencia:add_licencia",
	"config:licencia:delete_licencia",

	"config:regimen:view_regimen",
	"config:regimen:change_regimen",
	"config:regimen:add_regimen",
	"config:regimen:delete_regimen",

	"config:agente:view_situacion",
	"config:agente:change_situacion",
	"config:agente:add_situacion",
	"config:agente:delete_situacion",

	"config:guardia_lote:view_lote",
	"config:guardia_lote:change_lote",
	"config:guardia_lote:add_lote",
	"config:guardia_lote:delete_lote",

	"config:guardia_periodo:view_periodo",
	"config:guardia_periodo:change_periodo",
	"config:guardia_periodo:add_periodo",
	"config:guardia_periodo:delete_periodo",
];

perms_director = [
	"agentes:agente:view_agente",
	"agentes:historia:view_historialaboral",
	"agentes:nota:view_nota",
	"agentes:ausentismo:view_ausentismo",

	"partes:parte:view_parte",
	"partes:reporte:fichadas",
	"partes:reporte:partes",

	"reportes:agente:legajo_agentes",
	"reportes:agente:listado_agentes",
	"reportes:ausentismo:ausentismo",
	"reportes:ausentismo:ausentismo_totales",
	"reportes:ausentismo:licencias",

	"seguridad:usuario:view_usuario",
	"seguridad:rol:view_rol",

	"config:articulo:view_articulo",
	"config:feriado:view_feriado",
	"config:licencia:view_licencia",
	"config:regimen:view_regimen",
	"config:agente:view_situacion",
	"config:guardia_lote:view_lote",
	"config:guardia_periodo:view_periodo",
];

perms_jefe = [
	"agentes:agente:view_agente",
	"agentes:ausentismo:view_ausentismo",

	"partes:parte:add_parte",
	"partes:parte:change_parte",
	"partes:parte:confirmar_parte",
	"partes:reporte:fichadas",
	"partes:reporte:partes",

	"reportes:ausentismo:ausentismo",
	"reportes:ausentismo:ausentismo_totales",
	"reportes:ausentismo:licencias",
];

perms_personal = [
	"agentes:agente:view_agente",
	"agentes:ausentismo:view_ausentismo",
	"agentes:ausentismo:change_ausentismo",
	"agentes:ausentismo:delete_ausentismo",
	"agentes:historia:add_historialaboral",
	"agentes:historia:change_historialaboral",
	"agentes:nota:add_nota",
	"agentes:nota:change_nota",

	"partes:parte:procesar_parte",

	"reportes:agente:legajo_agentes",
	"reportes:agente:listado_agentes",
	"reportes:ausentismo:ausentismo",
	"reportes:ausentismo:ausentismo_totales",
	"reportes:ausentismo:licencias",
];

perms_personal_admin = perms_personal.concat([
	"agentes:historia:delete_historialaboral",
	"agentes:nota:delete_nota",
	"agentes:agente:view_fichado",
	"agentes:agente:habilitar_fichado",
	"agentes:agente:inhabilitar_fichado",
]);

perms_rrhh = [
	"agentes:agente:add_agente",
	"agentes:agente:view_agente",
	"agentes:ausentismo:view_ausentismo",
	"agentes:ausentismo:change_ausentismo",
	"agentes:historia:add_historialaboral",
	"agentes:historia:change_historialaboral",
	"agentes:nota:add_nota",
	"agentes:nota:change_nota",
];

perms_rrhh_admin = perms_rrhh.concat([
	"agentes:agente:baja_agente",
	"agentes:historia:delete_historialaboral",
]);

perms_rrhh_consulta = ["agentes:agente:view_agente"];

const roles = {
	rol_superadmin: {
		nombre: "Administrador General",
		permisos: permisos_all,
	},
	rol_jefe_servicio: {
		nombre: "Jefe de Servicio",
		permisos: perms_jefe,
	},
	rol_jefe_dpto: {
		nombre: "Jefe de Departamento",
		permisos: perms_jefe,
	},
	rol_jefe_seccion: {
		nombre: "Jefe de Sección",
		permisos: perms_jefe,
	},
	rol_jefe_sector: {
		nombre: "Jefe de Sector",
		permisos: perms_jefe,
	},
	rol_director: {
		nombre: "Director",
		permisos: perms_director,
	},
	rol_personal: {
		nombre: "Personal",
		permisos: perms_personal,
	},
	rol_personal_admin: {
		nombre: "Personal Admin",
		descripcion:
			"Permite ademas eliminar las notas o historia laboral de un agente",
		permisos: perms_personal_admin,
	},
	rol_rrhh: {
		nombre: "Recursos Humanos",
		permisos: perms_rrhh,
	},
	rol_rrhh_admin: {
		nombre: "Recursos Humanos Admin",
		permisos: perms_rrhh_admin,
	},
	rol_rrhh_consulta: {
		nombre: "Recursos Humanos Consulta",
		permisos: perms_rrhh_consulta,
	},
};

module.exports = {
	roles: roles,
	permisos_all: permisos_all,
};
