const { sqlDB } = require('./db');
const api = require('./api');
const utils = require('./utils');
const logger = require('./logger').logger;

async function migrateAuxiliarTables(){
    await migrateCausasBaja();
}

async function migrateCausasBaja(){
    const schema = 'CausasBaja'
    try {
        logger.debug(`Inicio migracion de ${schema}!!!`);
        const request = sqlDB.request();
        const result = await request.query`
            SELECT Causa
            FROM Personal_Causas_de_baja`;
        if (result.recordset && result.recordset.length){
            for (const row of result.recordset) {
                await parseCausaBaja(row);
            }
        }
        logger.debug(`Fin migracion de ${schema}!!!`);
        return;
    } catch (err) {
        logger.error(err);
    }
}


async function parseCausaBaja(row){
    return await utils.parseObject('causaBaja', { nombre:row.Causa },
        {
            nombre:row.Causa
        } )
}

async function migrateBajasAgente(agente) {
    let bajas = [];
    try {
        const request = sqlDB.request();
        const result = await request.query`
            SELECT 
                Agente,
                Fecha,
                Causa,
                NormalLegal_Tipo,
                NormalLegal_Numero,
                Observacion
            FROM Personal_Bajas_de_agentes
            WHERE Agente = ${agente.numero}
            ORDER BY Fecha`;
        if (result.recordset[0]){
            for (const row of result.recordset) {
                baja = await parseBajaAgente(row);
                bajas.push(baja);
            }
        }
        
        return bajas;
    } catch (err) {
        logger.error(err);
    }
}

async function parseBajaAgente(row){
    let motivo = await parseCausaBaja(row);
    let tipoNorma = await parseTipoNormaLegal(row.NormalLegal_Tipo);
    let baja = {
        fecha: utils.parseDate(row.Fecha),
        motivo: motivo,
        normaLegal: {
            tipoNormaLegal: tipoNorma,
            numeroNormaLegal: row.NormalLegal_Numero,
            observaciones: row.Observacion
        },
    }
    return baja;
}


async function parseTipoNormaLegal(nombre){
    if (utils.isEmpty(nombre)) return null;
    let tiposNormas = await api.getTiposNormaLegal(nombre);
    if (!tiposNormas.length ){
        tipoNorma = await api.addTipoNormalLegal({nombre:nombre });
    }
    else{
        tipoNorma = tiposNormas[0];
    }
    return tipoNorma;
}



module.exports = {
    migrateAuxiliarTables: migrateAuxiliarTables,
    migrateBajasAgente: migrateBajasAgente
}