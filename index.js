const logger = require("./logger").logger;
const { sqlDB, mongoDB, connectMongoose } = require("./db");
const auth = require("./auth/auth");
const agentes = require("./migracionAgente");
const historial = require("./migracionCargoAgente");
const ausentismo = require("./migracionAusencias");
const bajas = require("./migracionBajaAgente");
const partes = require("./migracionPartes");
const config = require("./config");
const preMigration = require("./preMigracion");

async function migrate() {
	try {
		logger.info("Inicio Proceso de Migracion. Version: 5.0");
		await sqlDB.connect();
		logger.debug("SQLServer Connection [OK]");
		await mongoDB.connect();
		logger.debug("MongoDB Connection [OK]");
		await connectMongoose();
		logger.debug("Mongoose Connection [OK]");
		await preMigration.dropMongoDB();
		await auth.createUserMigration();
		await auth.login(
			config.userMigration.user,
			config.userMigration.password
		);
		await bajas.migrateAuxiliarTables();
		await historial.migrateAuxiliarTables();
		await ausentismo.migrateAuxiliarTables();
		await partes.migrateAuxiliarTables();
		await agentes.migrateAuxiliarTables();
		await agentes.migrateAgentes();
		await partes.migrateParte();
		await auth.createRoles();
		await auth.createUsers();
		await auth.updateStatusJefes();
		logger.info("Fin Proceso de Migracion:");
		process.exit(0);
	} catch (error) {
		logger.error(error);
		logger.info("Fallo General!!! Revisar los logs!");
		process.exit(1);
	}
}

migrate();
