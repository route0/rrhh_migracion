const users = [
	{
		numero: "480008", // LAMMEL, ADRIAN ERWIN
		rol: "rol_director",
	},
	{
		numero: "12775", //", //  23117218 ZANCARINI, CESAR ANTONIO
		rol: "rol_director",
	},
	{
		numero: "185810", // DNI 16305746 CATTANEO, MARCOS ADRIAN
		rol: "rol_director",
	},
	{
		numero: "391026", // DNI 21850262 GOLDMAN ROTA, ADELAIDA ANTONIA ANALIA
		rol: "rol_director",
	},
	{
		numero: "567713", //", //  26509833	MICCI, LAURA DANIELA
		rol: "rol_jefe_dpto",
	},
	{
		numero: "358561", // DNI 21384524	GANEM, ELIAS JOSE
		rol: "rol_jefe_dpto",
	},
	{
		numero: "10015", // DNI 20875942	AGUILAR, MARCOS EDUARDO
		rol: "rol_jefe_dpto",
	},
	{
		numero: "846238", // DNI 27987377	SERNA, JORGELINA BEATRIZ
		rol: "rol_jefe_dpto",
	},
	{
		numero: "549300", // DNI 24091965	MEDINA, JORGE EDUARDO
		rol: "rol_jefe_dpto",
	},
	{
		numero: "875598", // DNI 17199532	TAMBURINI, CECILIA
		rol: "rol_jefe_dpto",
	},
	{
		numero: "684764", // DNI 26144775	PEREZ, MONICA IRENE
		rol: "rol_jefe_dpto",
	},
	{
		numero: "493435", // DNI 17025618	LEON, CLAUDIA PATRICIA
		rol: "rol_jefe_dpto",
	},
	{
		numero: "187260", // DNI 18066858	CAVALLARO, PATRICIA A.
		rol: "rol_jefe_dpto",
	},
	{
		numero: "924922", // DNI 26144765	VAZQUEZ, MARIANA JUDIT
		rol: "rol_jefe_dpto",
	},
	{
		numero: "39685", // DNI 21490772	ANDRES, ROMAN LAZARO
		rol: "rol_jefe_dpto",
	},
	{
		numero: "691011", // DNI 13737331	PETRICIO, ROBERTO JOSE
		rol: "rol_jefe_dpto",
	},
	{
		numero: "164917", // DNI 20989272	CARRASCAL PASCUAL, MARCELO DANIEL
		rol: "rol_jefe_dpto",
		// rol: "rol_superadmin",
	},
	{
		numero: "625383", // DNI 20752101	NINNO, JORGE NICOLAS
		rol: "rol_jefe_dpto",
	},
	// JEFES DE SERVICIO
	{
		numero: "138630", // DNI20465489	CACERES, RICARDO WASHINGTON
		rol: "rol_jefe_servicio",
	},
	{
		numero: "10737", // DNI34934522	BOTTA , MARIANO ANDRES
		rol: "rol_jefe_servicio",
	},
	{
		numero: "102968", // DNI21895667	BETANCUR, MARIELA EDITH
		rol: "rol_jefe_servicio",
	},
	{
		numero: "586685", // DNI17626307	MONTORO, GRISELDA NOEMI
		rol: "rol_jefe_servicio",
	},
	{
		numero: "75134", // DNI28361958	BARBOSA, FERNANDO MARTIN
		rol: "rol_jefe_servicio",
	},
	{
		numero: "550712", // DNI28617169	MELIHUEN, MONICA MARIEL
		rol: "rol_jefe_servicio",
	},
	{
		numero: "637077", // DNI21357724	OLIVAREZ, JOSE EDUARDO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "521475", // DNI24517966	MALSAM, MARIA LAURA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "519008", // DNI18222558	MAC DONNELL , MARIA CELINA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "634324", // DNI26178591	NUÑEZ AVENDAÑO, CECILIA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "14425", // DNI28945707	CHARRO, MARIA LUZ
		rol: "rol_jefe_servicio",
	},
	{
		numero: "783458", // DNI20548633	ROJAS, RICARDO JAVIER
		rol: "rol_jefe_servicio",
	},
	{
		numero: "376896", // DNI25854385	GAY, HUGO ALBERTO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "302469", // DNI18279003	FARRELL, MARIA ALEJANDRA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "5084", // DNI26805605	POCHAT, CECILIA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "254351", // DNI24522522	DE CARLI, CLAUDIO FABIAN
		rol: "rol_jefe_servicio",
	},
	{
		numero: "578395", // DNI17106049	MOLINI, WALTER
		rol: "rol_jefe_servicio",
	},
	{
		numero: "704651", // DNI17868669	PINTO, MARGARITA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "969824", // DNI28621149	ZUÑIGA, SILVIA MABEL
		rol: "rol_jefe_servicio",
	},
	{
		numero: "62347", // DNI23098836	ASTUDILLO, CARLOS ADRIAN
		rol: "rol_jefe_servicio",
	},
	{
		numero: "144735", // DNI18395696	CAMPETELLA, GUILLERMO A.
		rol: "rol_jefe_servicio",
	},
	{
		numero: "661355", // DNI26999322	PARADA, GABRIELA CRISTINA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "528885", // DNI20280074	MARIPIL, GLADYS INES
		rol: "rol_jefe_servicio",
	},
	{
		numero: "739398", // DNI10208308	RAMAT, AMANDA MARIA E.
		rol: "rol_jefe_servicio",
	},
	{
		numero: "359043", // DNI17416419	GARBE, MONICA ADRIANA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "66149", // DNI17268909	AVILA, SILVIA ADELA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "473945", // DNI10690145	KOLAR, RODOLFO MARCELO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "267507", // DNI16455929	DIAZ, GRACIELA ESTER
		rol: "rol_jefe_servicio",
	},
	{
		numero: "16602", // DNI32203882	MORENO, JUAN SANTIAGO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "235595", // DNI16816304	CORREA URANGA, HORACIO O.
		rol: "rol_jefe_servicio",
	},
	{
		numero: "591616", // DNI17107751	MORALES, CARLOS ADRIAN
		rol: "rol_jefe_servicio",
	},
	{
		numero: "655050", // DNI14536488	PAEZ, GRACIELA MARIA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "741556", // DNI14560029	RAMIREZ, JORGE HORACIO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "850413", // DNI20336957	SILVA, MARIA IRMA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "705027", // DNI23918723	PINTOS, LUIS ALFREDO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "533971", // DNI18079887	MARTINEZ, MARIA CAROLINA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "542934", // DNI22186699	MATO, IVANA RUTH
		rol: "rol_jefe_servicio",
	},
	{
		numero: "72779", // DNI28980662	BALBO, NOELIA ALEJANDRA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "542915", // DNI17868877	MATKOVICH, LILIANA BEATRIZ DEL CARMEN
		rol: "rol_jefe_servicio",
	},
	{
		numero: "533963", // DNI27719702	MARTINEZ, MARIA INES
		rol: "rol_jefe_servicio",
	},
	{
		numero: "361140", // DNI24899801	GARCIA, EMILIANO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "56322", // DNI21518922	ARINGOLI, JUAN JOSE
		rol: "rol_jefe_servicio",
	},
	{
		numero: "816500", // DNI14569086	SANJUAN, OSVALDO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "503939", // DNI18741106	LOPEZ KUTCHER, CRISTIAN EDGARDO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "557302", // DNI18796716	MENDEZ VASQUEZ, ERIKA INES
		rol: "rol_jefe_servicio",
	},
	{
		numero: "962744", // DNI20399527	ZANGARA, EDUARDO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "850597", // DNI31595723	SILVEIRA, PABLO MIGUEL
		rol: "rol_jefe_servicio",
	},
	{
		numero: "583163", // DNI18635604	MONTECINOS, SERGIO ANGEL
		rol: "rol_jefe_servicio",
	},
	{
		numero: "524027", // DNI17641044	MANTILARO, MAXIMO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "873150", // DNI31456845	SUAREZ, SUSANA CAROLINA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "19563", // DNI20933087	ALEGRIA, SILVIA LILIANA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "161647", // DNI33476917	CARDOSO, JESSICA RUBI
		rol: "rol_jefe_servicio",
	},
	{
		numero: "104003", // DNI23939179	BETAS, RICARDO HECTOR
		rol: "rol_jefe_servicio",
	},
	{
		numero: "517095", // DNI13747038	LUGO, SANTIAGO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "402551", // DNI24659523	GONZALEZ, RUBEN DARIO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "495409", // DNI22619143	LESCURA, NERINA IVANA
		rol: "rol_jefe_servicio",
	},
	{
		numero: "470098", // DNI21559554	KALTENBACH, GERMAN HUGO
		rol: "rol_jefe_servicio",
	},
	{
		numero: "375829", // DNI20004542	GATTI, MONICA M.
		rol: "rol_jefe_servicio",
	},
	{
		numero: "134774", // DNI22116232	CASTILLO, ANDREA FRANCISCA
		rol: "rol_jefe_servicio",
	},
	// JEFES DE SECTOR

	{
		numero: "339641", // DNI 29584054	FRACCAROLI, ALFREDO FERNANDO
		rol: "rol_jefe_sector",
	},
	{
		numero: "850024", // DNI 21378396	SILVA, CARMEN CELIA
		rol: "rol_jefe_sector",
	},
	{
		numero: "821705", // DNI 27212456	SANCHEZ, NICOLAS EMILIO
		rol: "rol_jefe_sector",
	},
	{
		numero: "429099", // DNI 23494440	HAEDO, ANGELICA SONIA NOEMI
		rol: "rol_jefe_sector",
	},
	{
		numero: "5906", // DNI 18689812	ACUÑA, MARIA ESTHER MAGALI
		rol: "rol_jefe_sector",
	},
	{
		numero: "348744", // DNI 21898790	FUMALE, MARIELA VIVIANA
		rol: "rol_jefe_sector",
	},
	{
		numero: "480052", // DNI 26704091	LAMOT, JUAN MANUEL
		rol: "rol_jefe_sector",
	},
	{
		numero: "790067", // DNI 31157248	ROMERO, VICTORIA GUADALUPE
		rol: "rol_jefe_sector",
	},
	{
		numero: "522985", // DNI 22287616	MANQUECOY, MARIA ROSA
		rol: "rol_jefe_sector",
	},
	{
		numero: "634497", // DNI 18180198	OJEDA, MIRTA ANDREA
		rol: "rol_jefe_sector",
	},
	{
		numero: "941872", // DNI 16816969	VILLALBA, MABEL SOLANGE
		rol: "rol_jefe_sector",
	},
	{
		numero: "790908", // DNI 18028616	ROSA, CLAUDIA ADRIANA
		rol: "rol_jefe_sector",
	},
	{
		numero: "22568", // DNI 32613932	ALMENDRA CONA, DANIELA NOEMI
		rol: "rol_jefe_sector",
	},
	{
		numero: "41643", // DNI 31524375	ANTIPAN, VANESA YANET
		rol: "rol_jefe_sector",
	},
	{
		numero: "731775", // DNI 28558763	QUIROGA, GABRIELA NOEMÍ
		rol: "rol_jefe_sector",
	},
	{
		numero: "648842", // DNI 27120955	OSOVNIKAR, CRISTIAN GUSTAVO
		rol: "rol_jefe_sector",
	},
	{
		numero: "919289", // DNI 32489618	VARGAS, NOELIA NOEMI
		rol: "rol_jefe_sector",
	},
	{
		numero: "889481", // DNI 26108066	TORO, MARCIA ANDREA
		rol: "rol_jefe_sector",
	},
	{
		numero: "131841", // DNI 30093491	BUSTOS, GONZALO FABIAN
		rol: "rol_jefe_sector",
	},
	{
		numero: "754929", // DNI 17618281	REYES, SANDRA MERCEDES
		rol: "rol_jefe_sector",
	},
	{
		numero: "908667", // DNI 20793190	URRUTIA, ANAHI ANGELICA
		rol: "rol_jefe_sector",
	},
	{
		numero: "742386", // DNI 26362904	RAMIREZ, LORENA VIVIANA
		rol: "rol_jefe_sector",
	},
	{
		numero: "541993", // DNI 20793816	MATAMALA, ANIBAL C.
		rol: "rol_jefe_sector",
	},
	{
		numero: "397216", // DNI 23220663	GONZALEZ, GLADYS NIEVES
		rol: "rol_jefe_sector",
	},
	{
		numero: "799222", // DNI 21151000	RUIZ DIAZ, GABRIELA NOEMI
		rol: "rol_jefe_sector",
	},
	{
		numero: "685923", // DNI 25441581	PEREZ VERDERA, PALMIRA VICTORIA
		rol: "rol_jefe_sector",
	},
	{
		numero: "252179", // DNI 21487444	D'ATRI, VERONICA GABRIELA
		rol: "rol_jefe_sector",
	},
	{
		numero: "464816", // DNI 13962951	JAYAT, GUSTAVO MANUEL
		rol: "rol_jefe_sector",
	},
	{
		numero: "756093", // DNI 23550570	RICOTTO, JUAN CARLOS
		rol: "rol_jefe_sector",
	},
	{
		numero: "42248", // DNI 18436012	ANTONINI, MARCOS GUSTAVO
		rol: "rol_jefe_sector",
	},
	{
		numero: "440030", // DNI 27739578	HILGEMBERG, SEBASTIAN ALBERTO
		rol: "rol_jefe_sector",
	},
	{
		numero: "858154", // DNI 31456803	SOSA, MARTIN GERMAN
		rol: "rol_jefe_sector",
	},
	{
		numero: "440199", // DNI 29860768	HITA, LUCIANA PATRICIA
		rol: "rol_jefe_sector",
	},
	{
		numero: "138684", // DNI 27932176	CACERES, ROSA CARINA
		rol: "rol_superadmin",
	},
	{
		numero: "40768", // DNI 29547320	ANTILEO, AUGUSTO SEBASTIAN JESUS
		rol: "rol_jefe_sector",
	},
	{
		numero: "106241", // DNI 29011123	BISCHOF, JUAN JOSE
		rol: "rol_jefe_sector",
	},
	{
		numero: "470088", // DNI 20123186	KACEM, MARIAM DANIELA
		rol: "rol_jefe_sector",
	},
	{
		numero: "75141", // DNI 20899209	BARBOZA, ANDRES ANTONIO
		rol: "rol_jefe_sector",
	},
	{
		numero: "780831", // DNI 20377460	RODRIGUEZ URIZ, DARIO GABRIEL
		rol: "rol_jefe_sector",
	},
	{
		numero: "523357", // DNI 26793823	LOPEZ CORMENZANA, JUAN CARLOS
		rol: "rol_jefe_sector",
	},
	{
		numero: "255326", // DNI 29131685	DE LA TORRE VILLAGRA, LAURA ALEJANDRA
		rol: "rol_jefe_sector",
	},
	{
		numero: "641083", // DNI 20807707	ORELLANO, NESTOR LUIS
		rol: "rol_jefe_sector",
	},
	{
		numero: "567241", // DNI 28945662	MEZA, NATALIA SOLEDAD
		rol: "rol_jefe_sector",
	},
	{
		numero: "580690", // DNI 17914582	MONTALVA, BLANCA SUSANA
		rol: "rol_jefe_sector",
	},
	// JEFE DE SECCION

	{
		numero: "151019", // DNI 33621590	CANALES, ANIBAL LAUTARO
		rol: "rol_jefe_seccion",
	},
	{
		numero: "638378", // DNI 25725604	OLMO LAGOS, MARIANA ALEJANDRA
		rol: "rol_jefe_seccion",
	},
	{
		numero: "654243", // DNI 18890462	PADILLA BUENDIA, ZULEMA
		rol: "rol_jefe_seccion",
	},
	{
		numero: "500431", // DNI 22619358	LLANCAQUEO, JUAN ALBERTO
		rol: "rol_jefe_seccion",
	},
	{
		numero: "902252", // DNI 30882447	VARGAS NAHUELANCA, MARIELA CRISTINA
		rol: "rol_jefe_seccion",
	},
	{
		numero: "42026", // DNI 24825649	ANTON, FLORINDA DEL CARMEN
		rol: "rol_jefe_seccion",
	},
	{
		numero: "825175", // DNI 30999536	SANDOVAL, ENZO JAVIER
		rol: "rol_jefe_seccion",
	},
	{
		numero: "114597", // DNI 24413381	BORQUEZ, SILVIA ANDREA
		rol: "rol_jefe_seccion",
	},
	{
		numero: "432597", // DNI 16816345	HERNANDEZ, CRISTINA BEATRIZ
		rol: "rol_jefe_seccion",
	},
	{
		numero: "17173", // DNI 25655316	ALCAZAR, CLAUDIO JORGE
		rol: "rol_jefe_seccion",
	},
	{
		numero: "442739", // DNI 22920577	HUENTEMILLA, ALDO GERMAN
		rol: "rol_jefe_seccion",
	},
	{
		numero: "706825", // DNI 16671472	PIUTRIN, CECILIA BEATRIZ
		rol: "rol_jefe_seccion",
	},
	{
		numero: "259690", // DNI 25113660	DEL RIO, LAURA PATRICIA
		rol: "rol_jefe_seccion",
	},
	// GESTION DE PERSONAL
	{
		numero: "239316", // DNI	22569842	COTAL, LUCILA
		rol: "rol_personal",
	},
	{
		numero: "366345", // DNI	14299941	GARCIA, SILVIA BEATRIZ
		rol: "rol_personal",
	},
	{
		numero: "682556", // DNI	13574125	PEREZ PIJOAN, MARIA SOLEDAD
		rol: "rol_personal",
	},
	{
		numero: "553790", // DNI	20196441	MELO, MONICA BEATRIZ
		rol: "rol_personal",
	},

	{
		numero: "278208", // DNI	29795944	DUARTE, MARIA DE LOS ANGELES
		rol: "rol_personal_admin",
	},
	{
		numero: "753634", // DNI	32577273	REVA, ANA PAULA
		rol: "rol_personal_admin",
	},
	{
		numero: "460192", // DNI	28213526	JARA, MARCELA ADRIANA
		rol: "rol_personal_admin",
	},
	{
		numero: "520286", // DNI	31099159	MAIROTTA, CINTI ATAMARA PATRICIA
		rol: "rol_personal_admin",
	},
	// RRHH
	{
		numero: "11648", // DNI 	34522165	ALARCON, MARIA FERNANDA
		rol: "rol_rrhh",
	},
	{
		numero: "165572", // DNI	21952601	CARRASCO, DORA LILIANA
		rol: "rol_rrhh_admin",
	},
	{
		numero: "812034", // DNI	36368880	SALAZAR, NOELIA AILIN
		rol: "rol_rrhh_admin",
	},
	{
		numero: "836627", // DNI	23718485	SEGUEL, LIDIA INES
		rol: "rol_rrhh_consulta",
	},
	// {
	//     usuario: "26810671",
	//     password: "26810671",
	//     nombre: "EDGARDO ARIEL",
	//     apellido: "KRISTENSEN",
	//     permisos: permisos_all,
	// },
];

module.exports = {
	users: users,
};
