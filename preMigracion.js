const { mongoDB } = require('./db');
const config = require('./config');
const logger = require('./logger').logger;


async function dropMongoDB(){
    logger.debug("Trying to drop MongoDB.");
    const database = mongoDB.db(config.db.mongo.database);
    await database.dropDatabase();
    logger.debug("MongoDB Drop Database [OK].");
}

module.exports = {
    dropMongoDB: dropMongoDB
}