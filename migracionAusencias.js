const { sqlDB } = require("./db");
const api = require("./api");
const logger = require("./logger").logger;
const utils = require("./utils");
const controller = require("./schemas/controller");

async function migrateAuxiliarTables() {
	await migrateArticulos();
	await migrateFeriados();
}

async function migrateFeriados() {
	const schema = "Feriados";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT
                Fecha
            FROM Personal_Feriados`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseFeriado(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateArticulos() {
	const schema = "Articulos";
	try {
		logger.debug(`Inicio migracion de ${schema}!!!`);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                Articulo,
                id,
                nombre,
                Descripcion,
                Grupo,
                Limitado,
                [Necesita info adicional] NecesitaInfo,
                [Titulo info adicional] TituloInfo,
                Codigo_OTI
            FROM Personal_Articulos`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseArticulo(row);
			}
		}
		logger.debug(`Fin migracion de ${schema}!!!`);
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateFrancosAgente(agente) {
	try {
		logger.debug(
			`Migrando francos del agente ${agente.apellido}, ${agente.nombre}`
		);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT
                Agente,
                Fecha
            FROM Personal_Francos
            WHERE Agente = ${agente.numero}`;
		if (result.recordset && result.recordset.length) {
			// for (const row of result.recordset) {
			// 	await parseFrancoAgente(row, agente);
			// }
			await parseFrancosAgente(result.recordset, agente);
		}
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateIndicadoresAgente(agente) {
	try {
		const agentes = await api.getData("agente", { numero: agente.numero });
		agente = agentes[0];
		const articulos = await api.getData("articulo", { codigo: 53 });
		logger.debug(
			`Migrando indicadores del agente ${agente.apellido}, ${agente.nombre}`
		);
		const request = sqlDB.request();
		const result = await request.query`
            SELECT
                Agente,
                [Año] Anio,
                [Dias asignados] Asignados,
                [Dias tomados] Tomados,
                [Art 96L tomados] Tomados96L,
                Vencido
            FROM Personal_Licencia_53
            WHERE Agente = ${agente.numero}`;
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				await parseIndicadoresAgente(row, agente, articulos[0]);
			}
		}
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateAusenciasAgente(agente) {
	try {
		logger.debug(`Migrando ausencias del agente nro ${agente.numero}`);
		let ausentismos = [];
		const request = sqlDB.request();
		const result = await request.query`
            SELECT 
                Fecha,
                Agente,
                Observacion,
                Adicional,
                Certificado,
                Extra,
                ausentismo.[Articulo] AusentismoArticulo,
                articulo.[Articulo],
                articulo.[id],
                articulo.[nombre],
                articulo.[Descripcion],
                articulo.[Grupo],
                articulo.[Limitado],
                articulo.[Necesita info adicional] NecesitaInfo,
                articulo.[Titulo info adicional] TituloInfo,
                articulo.[Codigo_OTI]
            FROM Personal_Ausentismo ausentismo
            LEFT JOIN Personal_Articulos articulo ON (ausentismo.Articulo = articulo.Articulo)
            WHERE Agente = ${agente.numero}
            ORDER BY Fecha, AusentismoArticulo`;
		if (result.recordset && result.recordset.length) {
			let ausentismo;
			for (const row of result.recordset) {
				let ausencia = await parseAusencia(row, agente);
				ausentismos.push(ausencia);
				if (ausentismo) {
					if (esMismaAusencia(ausentismo, ausencia, row)) {
						ausentismo = addAusenciaToAusentismo(
							ausentismo,
							ausencia
						);
					} else {
						await api.addData("ausentismo", ausentismo);
						ausentismo = await parseAusentismoAgente(row, ausencia);
					}
				} else {
					ausentismo = await parseAusentismoAgente(row, ausencia);
				}
			}
			if (ausentismo) await api.addData("ausentismo", ausentismo);
		}
		return;
	} catch (err) {
		logger.error(err);
	}
}

async function migrateFormulasArticulo(articulo) {
	const art = articulo.replace("'", "''");
	formulas = [];
	try {
		const request = sqlDB.request();
		const query = `
            SELECT FormulaId,
                Prioridad,
                LOWER(Sexo) Sexo,
                LOWER(Operacion) Operacion,
                LOWER(Tipo) Tipo,
                Parametro1,
                Comparacion,
                Parametro2
            FROM Personal_Formulas
            WHERE Parametro1 LIKE '%${art}%'
            `;
		const result = await request.query(query);
		if (result.recordset && result.recordset.length) {
			for (const row of result.recordset) {
				let artsInFormula = row.Parametro1
					? row.Parametro1.split(",")
					: [];
				let arts = artsInFormula.filter((x) => x === art);
				if (arts.length == 1) {
					row.Parametro1 = arts[0];
					const formula = await parseFormulaArticulo(row);
					formulas.push(formula);
				} else {
					if (arts.length > 1) {
						const msj = `Problema con Formula de Articulo ${art}. Splitting: ${row.Parametro1} ${artsInFormula}. Filtering: ${arts}`;
						logger.error(msj);
					}
				}
			}
		}
		return formulas;
	} catch (err) {
		logger.error(err);
	}
}

async function parseIndicadoresAgente(row, agente, articulo) {
	let indicador = {
		agente: { _id: agente._id },
		articulo: { _id: articulo._id, codigo: articulo.codigo },
		vigencia: row.Anio,
		periodo: null,
		vencido: row.Vencido ? true : false,
		intervalos: [
			{
				totales: row.Asignados,
				ejecutadas: row.Tomados,
				ejacutadas96L: row.Tomados96L,
			},
		],
	};
	await api.addData("indicador", indicador);
}

async function parseFeriado(row) {
	let feriado = { fecha: utils.parseDate(row.Fecha) };
	feriado = await api.addData("feriado", feriado);
	return feriado;
}

async function parseFrancoAgente(row, agente) {
	let franco = {
		agente: { _id: agente._id },
		fecha: utils.parseDate(row.Fecha),
	};
	franco = await api.addData("franco", franco);
	return franco;
}

async function parseFrancosAgente(rows, agente) {
	let francos = [];
	for (const row of rows) {
		let franco = {
			agente: { _id: agente._id },
			fecha: utils.parseDate(row.Fecha),
		};
		francos.push(franco);
	}
	return await controller.addFrancosAgentes(agente, francos);
}

async function parseFormulaArticulo(row) {
	let formula = {
		operacion: row.Operacion,
		parametro: row.Parametro2 ? row.Parametro2 : null,
		comparador: row.Comparacion ? row.Comparacion : null,
		sexo: row.Sexo,
		periodo: row.Tipo,
		limiteAusencias: row.Parametro2 ? row.Parametro2 : null,
		fechaDesde: null,
		fechaHasta: null,
		activa: true,
	};
	return formula;
}

async function parseArticulo(row) {
	let articulo = await api.getData("articulo", { codigo: row.Articulo });
	const esLicencia = ["21", "96L", "53"].includes(row.Articulo);
	// TODO Asignacion de colores

	if (articulo && articulo.length) {
		return articulo[0];
	} else {
		const formulas = await migrateFormulasArticulo(row.Articulo);
		let articulo = {
			idInterno: row.id,
			codigo: row.Articulo,
			nombre: row.nombre,
			descripcion: row.Descripcion ? row.Descripcion : "",
			grupo: row.Grupo ? row.Grupo : null,
			limitado: row.Limitado,
			requiereInformacionAdicional: row.NecesitaInfo,
			tituloInformacionAdicional: row.TituloInfo ? row.TituloInfo : "",
			codigoOTI: row.Codigo_OTI ? row.Codigo_OTI : "",
			formulas: formulas,
			// Otros
			diasCorridos: !esLicencia,
			diasHabiles: esLicencia,
			descuentaDiasLicencia: esLicencia,
		};
		articulo = await api.addData("articulo", articulo);
		return articulo;
	}
}

async function parseAusentismoAgente(row, ausencia) {
	let ausentismo = {
		agente: { _id: ausencia.agente._id },
		articulo: {
			_id: ausencia.articulo._id,
			codigo: ausencia.articulo.codigo,
			descripcion: ausencia.articulo.descripcion,
		},
		fechaDesde: ausencia.fecha,
		fechaHasta: ausencia.fecha,
		cantidadDias: 1,
		observacion: row.Observacion ? row.Observacion : "",
		adicional: row.Adicional ? row.Adicional : "",
		extra: row.Extra ? row.Extra : "",
		adjuntos: [],
		ausencias: [ausencia],
	};
	return ausentismo;
}

async function parseAusencia(row, agente) {
	let articulo = await controller.getArticuloByCodigo(row.Articulo);

	let ausencia;
	if (agente) {
		ausencia = {
			agente: { _id: agente._id },
			fecha: utils.parseDate(row.Fecha),
			articulo: { _id: articulo._id, codigo: articulo.codigo },
		};
	}
	return ausencia;
}

function addAusenciaToAusentismo(ausentismo, ausencia) {
	ausentismo.fechaHasta = ausencia.fecha;
	ausentismo.cantidadDias = ausentismo.cantidadDias + 1;
	ausentismo.ausencias.push(ausencia);
	return ausentismo;
}

function esMismaAusencia(ausentismo, ausencia, row) {
	const observacion = row.Observacion || "";
	if (
		JSON.stringify(ausentismo.articulo._id) ===
			JSON.stringify(ausencia.articulo._id) &&
		tomorrow(ausentismo.fechaHasta).getTime() == ausencia.fecha.getTime() &&
		ausentismo.observacion == observacion
	) {
		return true;
	}
	return false;
}

function tomorrow(date) {
	let tomorrow = new Date(date);
	return new Date(tomorrow.setDate(tomorrow.getDate() + 1));
}

module.exports = {
	migrateAuxiliarTables: migrateAuxiliarTables,
	migrateIndicadoresAgente: migrateIndicadoresAgente,
	migrateFrancosAgente: migrateFrancosAgente,
	migrateAusenciasAgente: migrateAusenciasAgente,
	migrateArticulos: migrateArticulos,
};
