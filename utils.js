const api = require('./api');

function isEmpty(obj){
    return obj === null || undefined
    ? true
    : (() => {
            for (const prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    return false;
                }
            }
            return true;
        })();
}

async function parseObject(objKey, queryParams, newObject){
    let objects = await api.getData(objKey, queryParams);
    if (!objects.length ){
        if (newObject){
            object = await api.addData(objKey, newObject);
        }
        else{
            object = null;
        }
        
    }
    else{
        object = objects[0];
    }
    return object;
}

function parseDate(date){
    let parsedDate = null;
    if (date) {
        const d1 = new Date(date)
        parsedDate = new Date(d1.getUTCFullYear(), d1.getUTCMonth(), d1.getUTCDate());
    }
    return parsedDate;
}

const timeout = ms => new Promise(res => setTimeout(res, ms));

module.exports = {
    isEmpty: isEmpty,
    parseObject: parseObject,
    parseDate: parseDate,
    timeout: timeout
}