const axios = require("axios");
const config = require("./config");
const logger = require("./logger").logger;

const apiURL = config.api.url;

let axiosConfig = {
	header: {
		"Content-Type": "multipart/form-data",
	},
};

async function login(user, password) {
	try {
		const response = await axios.post(apiURL + "auth/login", {
			usuario: user,
			password: password,
		});
		logger.debug("Login Status:" + response.status);
		axios.defaults.headers.common["Authorization"] =
			"JWT " + response.data.token;
		return response.status;
	} catch (err) {
		printError("login", err);
	}
}

async function sesion() {
	try {
		const response = await axios.get(apiURL + "auth/sesion");
		logger.debug("Login Sesion:" + response.status);
		return;
	} catch (err) {
		printError("sesion", err);
	}
}

async function saveAgente(agente) {
	try {
		const response = await axios.post(
			apiURL + "modules/agentes/agentes",
			agente,
			axiosConfig
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError("saveAgente", err);
	}
}

async function getPaises(nombrePais) {
	try {
		const response = await axios.get(apiURL + "core/tm/paises", {
			params: {
				nombre: nombrePais,
			},
		});
		const data = await response.data;
		return data;
	} catch (err) {
		printError("getPaises", err);
	}
}

async function addPais(pais) {
	try {
		const response = await axios.post(apiURL + "core/tm/paises", pais);
		const data = await response.data;
		return data;
	} catch (err) {
		printError("addPais", err);
	}
}

async function getEducacion(titulo) {
	try {
		const response = await axios.get(apiURL + "core/tm/educacion", {
			params: {
				titulo: titulo,
			},
		});
		const data = await response.data;
		return data;
	} catch (err) {
		printError("getEducacion", err);
	}
}

async function addEducacion(educacion) {
	try {
		const response = await axios.post(
			apiURL + "core/tm/educacion",
			educacion
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError("addEducacion", err);
	}
}

async function getTipoSituaciones(nombre) {
	try {
		const response = await axios.get(apiURL + "core/tm/tiposituaciones", {
			params: {
				nombre: nombre,
			},
		});
		const data = await response.data;
		return data;
	} catch (err) {
		printError("getSituaciones", err);
	}
}

async function addTipoSituacion(situacion) {
	try {
		const response = await axios.post(
			apiURL + "core/tm/tiposituaciones",
			situacion
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError("addSituacion", err);
	}
}

async function getTiposNormaLegal(nombre) {
	try {
		const response = await axios.get(apiURL + "core/tm/tiposnormalegal", {
			params: {
				nombre: nombre,
			},
		});
		const data = await response.data;
		return data;
	} catch (err) {
		printError("getTiposNormaLegal", err);
	}
}

async function addTipoNormalLegal(normaLegal) {
	try {
		const response = await axios.post(
			apiURL + "core/tm/tiposnormalegal",
			normaLegal
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError("addTipoNormalLegal", err);
	}
}

async function getObjects(url, params, desc = "getObjects") {
	try {
		const response = await axios.get(apiURL + url, {
			params: params,
		});
		const data = await response.data;
		return data;
	} catch (err) {
		printError(desc, err);
	}
}

async function addObject(url, newObject) {
	try {
		const response = await axios.post(
			apiURL + url,
			newObject,
			(desc = "addObject")
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError(desc, err);
	}
}

async function updateObject(url, updatedObject) {
	try {
		const response = await axios.put(
			apiURL + url,
			updatedObject,
			(desc = "updateObject")
		);
		const data = await response.data;
		return data;
	} catch (err) {
		printError(desc, err);
	}
}

urls = {
	agente: { url: "modules/agentes/agentes" },
	direccionCargo: { url: "core/organigrama/direcciones" },
	departamentoCargo: { url: "core/organigrama/departamentos" },
	servicioCargo: { url: "core/organigrama/servicios" },
	agrupamiento: { url: "core/organigrama/agrupamientos" },
	puesto: { url: "core/organigrama/puestos" },
	subpuesto: { url: "core/organigrama/subpuestos" },
	sector: { url: "core/organigrama/sectores" },
	ubicacionSubtipo: { url: "core/organigrama/ubicacionessubtipo" },
	ubicacion: { url: "core/organigrama/ubicaciones" },

	regimenHorario: { url: "core/tm/regimenhorarios" },
	pais: { url: "core/tm/paises" },
	localidad: { url: "core/tm/localidades" },
	provincia: { url: "core/tm/provincias" },
	causaBaja: { url: "core/tm/causabajas" },
	tipoSituacion: { url: "core/tm/tiposituaciones" },

	articulo: { url: "modules/ausentismo/articulos" },
	ausentismo: { url: "modules/ausentismo/ausencias" },
	indicador: { url: "modules/ausentismo/indicadores" },
	feriado: { url: "modules/ausentismo/feriados" },
	franco: { url: "modules/ausentismo/francos" },

	parteEstado: { url: "modules/partes/parteestados" },
	parteJustificacion: { url: "modules/partes/partejustificaciones" },
	ubicacionTipo: { url: "core/organigrama/ubicacionestipo" },
	parte: { url: "modules/partes/partes" },
	parteAgente: { url: "modules/partes/partesagentes" },
	fichada: { url: "modules/partes/fichadas" },
	fichadaCache: { url: "modules/partes/fichadascache" },

	usuario: { url: "auth/usuarios" },
	rol: { url: "modules/seguridad/roles" },
};

async function getData(urlKey, params, url) {
	if (url) {
		return await getObjects(url, params, `get${urlKey}`);
	} else {
		return await getObjects(urls[urlKey]["url"], params, `get${urlKey}`);
	}
}

async function addData(urlKey, params) {
	return await addObject(urls[urlKey]["url"], params, `add${urlKey}`);
}

async function updateData(urlKey, objectID, params) {
	return await updateObject(
		`${urls[urlKey]["url"]}/${objectID}`,
		params,
		`update${urlKey}`
	);
}

function printError(origen, err) {
	var msj = err.message || "";
	if (err && err.config) {
		// Axios Request extra info
		const url = err.config.url || "No URL";
		const data = err.config.data || "No Data";
		// msj = `${msj} REQUEST [url]: ${url} [data]:${data}`;
		msj = `${msj} REQUEST [url]: ${url} [data]:`;
	}
	if (err && err.response) {
		// API Response
		const status = err.response.status || "No Status";
		const dataResponse = JSON.stringify(err.response.data) || "No Data";
		msj = `${msj} RESPONSE [status]: ${status} [data]:${dataResponse}`;
	}
	logger.error(msj);
}

module.exports = {
	saveAgente: saveAgente,
	getEducacion: getEducacion,
	addEducacion: addEducacion,
	getPaises: getPaises,
	addPais: addPais,
	getTipoSituaciones: getTipoSituaciones,
	addTipoSituacion: addTipoSituacion,
	getTiposNormaLegal: getTiposNormaLegal,
	addTipoNormalLegal: addTipoNormalLegal,
	getData: getData,
	addData: addData,
	updateData: updateData,
	login: login,
	sesion: sesion,
};
