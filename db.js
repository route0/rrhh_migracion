const mongoClient = require("mongodb").MongoClient;
const mongoose = require("mongoose");
const sqlClient = require("mssql");
const logger = require("./logger").logger;
const config = require("./config");

// SQL SERVER CONFIG
const sqlConfig = {
	user: config.db.sqlserver.user,
	password: config.db.sqlserver.password,
	server: config.db.sqlserver.server,
	database: config.db.sqlserver.database,
	parseJSON: true,
	requestTimeout: 60000,
	connectionTimeout: 60000,
	pool: {
		max: 10,
		min: 0,
		idleTimeoutMillis: 30000,
	},
};

const sqlDB = new sqlClient.ConnectionPool(sqlConfig);

sqlDB.on("error", (err) => {
	logger.error(err);
});

// MONGODB CONFIG
const mongoConfig = {
	url: config.db.mongo.url,
	database: config.db.mongo.database,
	uri: config.db.mongo.uri,
};

const mongoDB = new mongoClient(mongoConfig.uri);

// MONGOOSE

mongoose.Promise = Promise;

mongoose.connection.on("connected", () => {
	logger.debug("MongoDB Connection Established");
});

mongoose.connection.on("reconnected", () => {
	logger.debug("MongoDB Connection Reestablished");
});

mongoose.connection.on("disconnected", () => {
	logger.error("MongoDB Connection Disconnected");
});

mongoose.connection.on("close", () => {
	logger.debug("MongoDB Connection Closed");
});

mongoose.connection.on("error", (error) => {
	logger.error("MongoDB ERROR: " + error);
});

async function connectMongoose() {
	await mongoose.connect(mongoConfig.url, {
		useNewUrlParser: true,
		autoReconnect: true,
		reconnectTries: 1000000,
		reconnectInterval: 3000,
	});
}

module.exports = {
	sqlDB: sqlDB,
	mongoDB: mongoDB,
	connectMongoose: connectMongoose,
};
