const api = require("../api");
const utils = require("../utils");
const users = require("./usuarios").users;
const { roles, permisos_all } = require("./roles_perms");
const logger = require("../logger").logger;

async function login(user, password) {
	let loginOK = false;
	while (!loginOK) {
		const status = await api.login(user, password);
		if (status == 200) {
			loginOK = true;
		} else {
			logger.debug("Retrying logging after 10 seg.");
			await utils.timeout(10000);
		}
	}
	return;
}

async function sesion() {
	return await api.sesion();
}

async function createUserMigration() {
	logger.debug("Inicio Creacion de Usuario de Migracion...");
	user = {
		usuario: "11111111",
		password: "11111111",
		nombre: "Usuario",
		apellido: "Migracion",
		permisos: permisos_all,
	};
	await api.addData("usuario", user);
}

async function createRoles() {
	logger.debug("Inicio Creacion de Roles...");
	const rolesKeys = Object.keys(roles);
	for (const key of rolesKeys) {
		let rol = roles[key];
		rol["codename"] = key; // Add codename to rol
		await api.addData("rol", rol);
	}
}

async function findAgenteByNumero(numero) {
	try {
		const agentes = await api.getData("agente", { numero: numero });
		if (agentes.length != 1)
			throw "Problemas con la cantidad de agentes retornados";
		return agentes[0];
	} catch (error) {
		logger.error(
			`Conflicto con el usuario buscado. Agente numero ${numero}`
		);
	}
}

async function findUbicacionesJefe(codigoUbicacion) {
	try {
		let url = `core/organigrama/ubicaciones/${codigoUbicacion}/children`;
		return await api.getData("ubicaciones", {}, url);
	} catch (error) {
		logger.error(
			`Problema al consultar las ubicaciones con codigo ${codigoUbicacion}`
		);
		return [];
	}
}

async function createUsers() {
	logger.debug("Inicio Creacion de Usuarios...");
	for (const user of users) {
		const agente = await findAgenteByNumero(user.numero);
		if (agente) {
			const rolName = user.rol;
			const rol = roles[rolName];
			const newUser = {
				usuario: agente.documento,
				// password: agente.documento,
				nombre: agente.nombre,
				apellido: agente.apellido,
				roles: [rolName],
				permisos: rol.permisos,
				authMethod: "ldap",
			};
			await api.addData("usuario", newUser);
		}
	}
}

async function updateStatusJefes() {
	logger.debug(
		"Inicio Actualizacion Permisos Jefes de Servicios/Dpto/Sector..."
	);
	let rolesJefe = [
		"rol_jefe_sector",
		"rol_jefe_seccion",
		"rol_jefe_dpto",
		"rol_jefe_servicio",
	];
	for (const user of users) {
		try {
			let ubicacionesJefe = [];
			const rolName = user.rol;
			if (rolesJefe.includes(rolName)) {
				const agente = await findAgenteByNumero(user.numero);
				if (agente) {
					const sector = agente.situacionLaboral.cargo.sector; // Lugar de Trabajo
					ubicacionesJefe = await findUbicacionesJefe(
						sector.ubicacion
					);
					let updatedAgente = {
						"situacionLaboral.cargo.esJefe": true,
						"situacionLaboral.cargo.jefeUbicaciones": ubicacionesJefe,
					};
					await api.updateData("agente", agente._id, updatedAgente);
				}
			}
		} catch (error) {
			logger.error(
				`Problema al actualizar los permisos como jefe de ${agente.apellido}, ${agente.nombre}`
			);
		}
	}
}

module.exports = {
	login: login,
	sesion: sesion,
	createUserMigration: createUserMigration,
	createRoles: createRoles,
	createUsers: createUsers,
	updateStatusJefes: updateStatusJefes,
};
